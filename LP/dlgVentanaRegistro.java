package LP;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import LN.clsGestorLN;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Ventana de registro que permite la creación de nuevos usuarios en el sistema.
 * Esta ventana hereda de JDialog y implementa ActionListener para manejar 
 * eventos de los botones.
 */
public class dlgVentanaRegistro extends JDialog implements ActionListener 
{

	/**
	 * Identificador único para la serialización.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Panel de contenido principal.
	 */
	private final JPanel contentPanel = new JPanel();
	
	/**
	 * Panel que contiene los componentes para aceptar o cancelar el registro, así como los campos de entrada de datos.
	 */
	private JPanel buttonPane;
	/**
	 * Botón para confirmar el registro.
	 */
	private JButton okButton;
	/**
	 * Botón para cancelar el registro.
	 */
	private JButton cancelButton;
	/**
	 * Campo de texto para introducir el DNI del usuario a registrar.
	 */
	private JTextField txtDNI;
	/**
	 * Campo de texto para introducir el nombre del usuario a registrar.
	 */
	private JTextField txtNombre;
	/**
	 * Campo de texto para introducir la nota del usuario a registrar.
	 */
	private JTextField txtNota;
	/**
	 * Campo de texto para introducir el nombre de usuario del nuevo usuario.
	 */
	private JTextField txtUser;
	/**
	 * Campo de texto para introducir la contraseña del nuevo usuario.
	 */
	private JTextField txtPass;
	/**
	 * Campo de texto para introducir el rol del nuevo usuario.
	 */
	private JTextField txtRol;

	
	//*********************** GESTOR LN **********************
	/**
	 * Objeto gestor de lógica de negocio.
	 */
	private clsGestorLN obj_gln;
	//*********************** GESTOR LN **********************
	
	/**
	 * Ventana de registro que permite la creación de nuevos usuarios en el sistema.
	 * 
	 * @param obj_gln El gestor de lógica de negocio utilizado para manejar las operaciones.
	 */
	public dlgVentanaRegistro(clsGestorLN obj_gln) 
	{

		//la ventana de login te pasa su gln a ti, para que sea el mismo
		this.obj_gln = obj_gln;

		setTitle("Ventana Registro");
		setBounds(100, 100, 450, 321);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		txtDNI = new JTextField();
		txtDNI.setBounds(192, 40, 86, 20);
		contentPanel.add(txtDNI);
		txtDNI.setColumns(10);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(192, 71, 86, 20);
		contentPanel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtNota = new JTextField();
		txtNota.setBounds(192, 102, 86, 20);
		contentPanel.add(txtNota);
		txtNota.setColumns(10);
		
		txtUser = new JTextField();
		txtUser.setBounds(192, 133, 86, 20);
		contentPanel.add(txtUser);
		txtUser.setColumns(10);
		
		txtPass = new JTextField();
		txtPass.setBounds(192, 164, 86, 20);
		contentPanel.add(txtPass);
		txtPass.setColumns(10);
		
		txtRol = new JTextField();
		txtRol.setBounds(192, 195, 86, 20);
		contentPanel.add(txtRol);
		txtRol.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("DNI");
		lblNewLabel.setBounds(105, 43, 77, 14);
		contentPanel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("NOMBRE");
		lblNewLabel_1.setBounds(105, 74, 77, 14);
		contentPanel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("NOTA");
		lblNewLabel_2.setBounds(105, 105, 77, 14);
		contentPanel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("USER");
		lblNewLabel_3.setBounds(105, 136, 77, 14);
		contentPanel.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("PASSWORD");
		lblNewLabel_4.setBounds(105, 167, 77, 14);
		contentPanel.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("ROL");
		lblNewLabel_5.setBounds(105, 198, 77, 14);
		contentPanel.add(lblNewLabel_5);

		buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);

		okButton = new JButton("OK");
		
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);


		//el texto que ve el humano
		cancelButton = new JButton("Cancel");
		
		//el texto que ve la maquina
		cancelButton.setActionCommand("Cancel");

		buttonPane.add(cancelButton);

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);



		okButton.addActionListener( this ) ;
		cancelButton.addActionListener( this ) ;


		setLocationRelativeTo(null);

		setVisible(true);

	}
	
	/**
	 * Maneja las acciones realizadas en la ventana.
	 * 
	 * @param e El evento de acción que desencadenó el método.
	 */
	public void actionPerformed(ActionEvent e) 
	{
		
		switch (e.getActionCommand()) 
		{
		
			case "OK": 
			{
				String dni = txtDNI.getText();
				String nombre = txtNombre.getText();
				double nota = Double.parseDouble(txtNota.getText());
				String user = txtUser.getText();
				String pass = txtPass.getText();
				int rol = Integer.parseInt( txtRol.getText() );
		
				if( obj_gln.int_obtenerCredencialesUserPass(user, pass) == -1 )
				{
					//sout
					JOptionPane.showMessageDialog(null, "Nuevo usuario!");
				}
				else{
					JOptionPane.showMessageDialog(null, "Ese usuario ya existe!!!");
				}
			

				
				break;
			}

			case "Cancel": 
			System.out.println("Cancel");
			dispose();
			break;
			
		}
		
	}
}