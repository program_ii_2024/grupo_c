package LP;

import java.util.ArrayList;

import COMUN.clsConstantes;
import COMUN.itfProperty;
import EXCEPCIONES.clsDNIFormatoIncorrecto;
import LN.clsCliente;
import LN.clsGestorLN;
import LN.clsVehiculo;



/**
* Clase que implementa el menu principal de la aplicacion, permitiendo acceder a
* diferentes funcionalidades.
* Utiliza la clase {@link clsGestorLN} como nexo de union entre las diferentes 
* capas del programa.
* 
* @author Samuel.Aranda
* @version 4.0
*/
public class clsMenu 
{

    /**
    * Objeto de la clase {@link clsGestorLN} utilizado para gestionar y 
    * manipular los datos de los usuarios y vehículos.
    */
    private clsGestorLN obj_gestorLN; // nexo de union entre capas

    /**
    * Constructor de la clase `clsMenu` que inicializa el objeto `obj_gestorLN`.
    */
    public clsMenu() 
    {
        obj_gestorLN = new clsGestorLN();
    }


    /**
    * Metodo principal que muestra el menu inicial de la aplicacion y redirige a
    * los submenus segun la opcion seleccionada. Además perimite que nuevos 
    * usuarios se registren, escogiendo sus nombres de usuario y contraseñas.
    */
    public void vo_menuInicio() 
    {
    boolean bln_registrado = false;
       do
       {
            vo_mostrarMenuRegistro();
            
       } while (bln_registrado == false);

        char op = ' ';
        do 
        {
            System.out.println("********** Bienvenido/a ***********");
            System.out.println("   Acceso a la aplicacion");
            System.out.println("*********************************");
            System.out.println("1. Menu cliente");
            System.out.println("2. Menu mecanico");
            System.out.println("3. Menu gerente");
            System.out.println("4. Volver atrás");
            System.out.println("---------------------------");
            System.out.println("0. Salir programa");
            System.out.println("---------------------------");

            System.out.print("Elegir Opcion: ");
            op = UtilidadesLP.leerCaracter();
            switch (op) 
            {
                case '1':
                    vo_mostrarMenuCliente();
                    break;
                case '2':
                    vo_mostrarMenuMecanico();
                    break;
                case '3':
                    vo_mostrarMenuGerente();
                    break;
                case '4':
                    vo_mostrarMenuRegistro();
                    break;
                case '0':
                    System.out.println("FIN");
                    break;
                default:
                    System.out.println("******** ERROR ***********");
                    System.out.println("No es una opcion correcta!!");
                    System.out.println("**************************");
            }
        } while (op != '0');
    }
    
    /**
    * Metodo privado que permite que los usuarios indiquen si estan o no 
    * registrados y en caso de no estarlo, le permite crear un nombre de
    * usuario y una contraseña. Añade a una lista los dni adjuntos a un usuario,
    * utilizandola despues para permitir el acceso a los submenus del programa.
    */
    private void vo_mostrarMenuRegistro() 
    {
        boolean bln_registrado;
        bln_registrado = false;

        System.out.println("Bienvenido/a!");

        do
        {
            System.out.println("¿Estás registrado/a? (si/no)");
            String str_respuesta = UtilidadesLP.leerCadena();

            if (str_respuesta.equalsIgnoreCase("si"))
            {
                bln_registrado = true;

            } else if (str_respuesta.equalsIgnoreCase("no"))
            {
                System.out.println("¡Regístrate!");
                System.out.println("Nombre de usuario:");
                String str_nombreUsuario = UtilidadesLP.leerCadena();
                System.out.println("DNI del usuario:");
                String str_dni = UtilidadesLP.leerCadena();
                System.out.println("Contraseña:");
                String str_contraseña = UtilidadesLP.leerCadena();
                bln_registrado = obj_gestorLN.bln_almacenarDNIRegistro(
                    str_nombreUsuario,str_dni, str_contraseña);
            } else 
            {
                System.out.println("Respuesta inválida. Responda si o no.");
            }

        }while (!bln_registrado);
    }

    /**
    * Metodo privado que muestra el menu para el usuario tipo gerente.
    * Permite acceder a funcionalidades especificas para la gestion 
    * administrativa del taller mecanico.
    */
    private void vo_mostrarMenuGerente() 
    {

        String str_usuario;
        String str_clave;

        System.out.println("Teclea usuario gerente:");
        str_usuario = UtilidadesLP.leerCadena();
        System.out.println("Teclea clave de acceso:");
        str_clave = UtilidadesLP.leerCadena();

        if (str_usuario.equals("admin") && str_clave.equals("admin"))
            vo_mostrarMenuAdministrador();
        else
            System.out.println("Credenciales incorrectas!!");

    }

    /**
    * Metodo privado que muestra el menu para el usuario tipo administrador.
    * Ofrece opciones para anadir, borrar, modificar y visualizar clientes y 
    * vehiculos.
    */
    private void vo_mostrarMenuAdministrador() 
    {

        char op;
        do
        {
            System.out.println("-------------------------------");
            System.out.println("MENU ADMIN TALLER MECANICO");
            System.out.println("-------------------------------");
            System.out.println("1.Insertar cliente");
            System.out.println("2.Insertar coche");
            System.out.println("3.Borrar cliente");
            System.out.println("4.Borrar vehiculo");
            System.out.println("5.Modificar cliente");
            System.out.println("6.Modificar vehiculo");
            System.out.println("7.Inicio");
            System.out.println("---------------------------");
            System.out.println("0. Salir programa");
            System.out.println("---------------------------");

            System.out.print("Elegir Opcion: ");
            op = UtilidadesLP.leerCaracter();
            switch (op) {
            
                case '1':
                    vo_menuAñadirCliente();
                    break;
                case '2':
                    vo_menuAñadirCoche();
                    break;
                case '3':
                    vo_menuBajaUsuario();
                    break;
                case '4':
                    vo_menuBajaVehiculo();
                    break;
                case '5':
                    vo_menuModificarUsuario();
                    break;
                case '6':
                    vo_menuModificarVehiculo();
                    break;
                case '7':
                    vo_menuInicio();
                    break;
                case '0':
                    System.out.println("FIN");
                    break;
                default:
                    System.out.println("******** ERROR ***********");
                    System.out.println("No es una opcion correcta!");
                    System.out.println("**************************");
                }
            } while (op != '0');
      
    }

    /**
    * Metodo privado que muestra el menu para el usuario tipo mecanico.
    * Verifica si el usuario esta registrado y muestra un menu simplificado con
    * opciones especificas para mecanicos.
    */
    private void vo_mostrarMenuMecanico() 
    {
        
        String str_usuario;
        String str_clave;
        String str_dni;
        boolean bln_respuesta;

        System.out.println("Teclea usuario mecánico:");
        str_usuario = UtilidadesLP.leerCadena();
        System.out.println("Teclea tu DNI:");
        str_dni = UtilidadesLP.leerCadena();
        System.out.println("Teclea clave de acceso:");
        str_clave = UtilidadesLP.leerCadena();

        bln_respuesta = 
        obj_gestorLN.bln_verificarCredencialesMecanico(str_usuario,
        str_dni, str_clave);

        if (bln_respuesta) 
        {

            vo_mostrarMenuMecanicoRegistrado();
           
        } else 
        {
            System.out.println("Error! No estás registrado o no eres mecánico" 
            + "del taller");
            vo_pausa();
        }
    }

    /**
    * Metodo privado que muestra el menu para el usuario registrado (mecanico).
    * Proporciona opciones para anadir clientes y vehiculos desde el menu 
    * principal.
    */
    private void vo_mostrarMenuMecanicoRegistrado() 
    {

        char op;
        do
        {
            System.out.println("-------------------------------");
            System.out.println("MENU MECANICO TALLER MECANICO");
            System.out.println("-------------------------------");
            System.out.println("1.Insertar cliente");
            System.out.println("2.Insertar vehiculo");
            System.out.println("3.Inicio");
            System.out.println("---------------------------");
            System.out.println("0. Salir programa");
            System.out.println("---------------------------");

            System.out.print("Elegir Opcion: ");
            op = UtilidadesLP.leerCaracter();
            switch (op) 
            {
            
                case '1':
                    vo_menuAñadirCliente();
                    break;
                case '2':
                    vo_menuAñadirCoche();
                    break;
                case '3':
                    vo_menuInicio();
                    break;
                case '0':
                    System.out.println("FIN");
                    break;
                default:
                    System.out.println("******** ERROR ***********");
                    System.out.println("No es una opcion correcta!");
                    System.out.println("**************************");
                }
        } while (op != '0');
    }

    /**
    * Metodo privado que muestra el menu para el usuario tipo cliente
    * Verifica si el usuario esta registrado y muestra un menu simplificado con
    * opciones especificas para clientes.
    */
    private void vo_mostrarMenuCliente() 
    {
        String str_usuario;
        String str_clave;
        String str_dni;
        boolean bln_respuesta;

        System.out.println("Teclea usuario cliente:");
        str_usuario = UtilidadesLP.leerCadena();
        System.out.println("Teclea tu DNI:");
        str_dni = UtilidadesLP.leerCadena();
        System.out.println("Teclea clave de acceso:");
        str_clave = UtilidadesLP.leerCadena();

        bln_respuesta = 
        obj_gestorLN.bln_verificarCredencialesCliente(str_usuario,
        str_dni, str_clave);

        
        if (bln_respuesta) 
        {
            vo_mostrarMenuClienteRegistrado();
    
        } else 
        {
            System.out.println("Error! No estás registrado o no eres ciente" 
            + " del taller");
            vo_pausa();
        }
    }

    /**
    * Metodo privado que muestra el menu para el usuario tipo cliente.
    * Permite visualizar diferentes listados de clientes y vehiculos.
    */
    private void vo_mostrarMenuClienteRegistrado() 
    {
        char op = '@';
        do 
        {
            System.out.println("---------------------------");
            System.out.println("MENU CLIENTE TALLER MECANICO");
            System.out.println("---------------------------");
            System.out.println("1.Visualizar clientes");
            System.out.println("2.Visualizar clientes ordenados por direccion"
            );
            System.out.println("3.Visualizar coches");
            System.out.println("4.Visualizar coches ordenados por matricula");
            System.out.println("---------------------------");
            System.out.println("5.Inicio");
            System.out.println("---------------------------");
            System.out.print("Opcion: ");

            op = UtilidadesLP.leerCaracter();

            switch (op) 
            {
                case '1':
                    vo_visualizarClientes();
                    break;
                case '2':
                    vo_visualizarClientesPorDireccion();
                    break;
                case '3':
                     vo_visualizarVehiculos();
                    break;
                case '4':
                    vo_visualizarCochesPorMatricula();
                    break;
                case '5':
                    vo_menuInicio();
                    break;
                default:
                    System.out.println("No es una opcion correcta!");
                    vo_pausa();
            }
        } while (op != '5');

    }


    /**
    * Metodo publico que muestra un menu para la gestion de usuarios.
    * Ofrece opciones para anadir, borrar, modificar y visualizar usuarios.
    */
    public void vo_menuAltaUsuario() 
    {
       
        char op;

        do 
        {
            op = UtilidadesLP.leerCaracter();

            switch (op) 
            {
                case 1:
                    vo_menuAñadirCliente();

                    break;
                case 2:
                    vo_menuBorrarCliente();
                    break;
                case 3:
                    vo_menuModificarUsuario();
                    break;
                case 4:
                    vo_menuVisualizarClientes();
                    break;
                case 5:
                    System.out.println("FIN");
                    break;
                default:
                    System.out.println("No es una opcion correcta!");
                    vo_pausa();
            }
        } while (op != 5);
    }

    /**
    * Metodo que muestra un mensaje de pausa en la consola y espera a que el
    * usuario presione INTRO para continuar.
    */
    private void vo_pausa() 
    {
        System.out.println("Pulsa <INTRO> para continuar...");
        UtilidadesLP.leerCadena();
    }

    /**
    * Metodo que permite borrar un cliente del sistema.
    * Recupera el nombre del cliente a eliminar y realiza la operacion con el
    * gestor correspondiente.
    */
    private void vo_menuBorrarCliente() 
    {
        boolean bln_r;
        String str_nombre;

        System.out.println("Teclea nombre jugador:");
        str_nombre = UtilidadesLP.leerCadena();

        bln_r = obj_gestorLN.vo_eliminarUsuario(str_nombre);

        if (bln_r) 
        {
            System.out.println("*****************************************");
            System.out.println("Usuario borrado del sistema con exito!");
            System.out.println("*****************************************");
            vo_pausa();
        } else 
        {
            System.out.println("******************************************");
            System.out.println("Ups,no hemos podido encontrar este usuario!");
            System.out.println("******************************************");
            vo_pausa();
        }
    }

    /**
    * Metodo que muestra la informacion de todos los clientes registrados en el
    * sistema.
    * Recupera la informacion de los clientes a traves del gestor y la muestra
    * en la consola.
    */
    private void vo_menuVisualizarClientes() 
    {
        ArrayList<String> obj_clientes = obj_gestorLN.
        arrl_recuperarInformacionUsuarios();

        if (obj_clientes.isEmpty()) 
        {
            System.out.println("-----------------------------");
            System.out.println("No hay clientes para mostrar");
            System.out.println("-----------------------------");
            vo_pausa();
        } else 
        {
            System.out.println("INFORMACION DE CLIENTES:");
            System.out.println("---------------------------");
            for (String str_cliente : obj_clientes) {

                System.out.println(str_cliente);
                System.out.println("----------------------");
            }
            vo_pausa();
        }
    }

    /**
    * Metodo que permite anadir un nuevo cliente al sistema.
    * Solicita los datos necesarios al usuario y realiza la operacion 
    * correspondiente con el gestor.
    */
    private void vo_menuAñadirCliente() 

    {
        String str_nombre;
        String str_apellido;
        String str_dni;
        String str_email;
        int int_telefono;
        int int_rol = 1;
        String str_contraseña = "";
        String str_nombreUsuario = "";

        System.out.println("Teclea nombre del cliente:");
        str_nombre = UtilidadesLP.leerCadena();

        System.out.println("Teclea apellido del cliente:");
        str_apellido = UtilidadesLP.leerCadena();

        System.out.println("Teclea DNI del cliente:");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("Teclea email del cliente:");
        str_email = UtilidadesLP.leerCadena();

        System.out.println("Teclea telefono del cliente:");
        int_telefono = UtilidadesLP.leerEntero();

        System.out.println("Teclea la direccion del cliente:");
        String str_direccion = UtilidadesLP.leerCadena();

        System.out.println("Teclea numero de cuenta del cliente:");
        String str_numVis = UtilidadesLP.leerCadena();

        try {
            obj_gestorLN.vo_insertarCliente(str_nombre, str_apellido, str_dni,
                    str_email, int_telefono, int_rol, str_contraseña, str_nombreUsuario,
                    str_direccion, str_numVis);

            boolean bln_resultado = obj_gestorLN.bln_almacenarDNICliente(str_dni);

            if (bln_resultado) {
                System.out.println("*****************************************");
                System.out.println("Excelente! Usuario añadido al sistema!");
                System.out.println("*****************************************");
                vo_pausa();
            } else {
                System.out.println("*****************************************");
                System.out.println("Error! Datos invalidos!");
                System.out.println("*****************************************");
                vo_pausa();
            }
        } catch (clsDNIFormatoIncorrecto e) {
            System.out.println("*****************************************");
            System.out.println("Error! DNI incorrecto!");
            System.out.println("*****************************************");
            vo_pausa();
        }
    }

     /**
    * Metodo publico que permite modificar un usuario existente.
    * Utiliza el objeto `obj_gestorLN` para realizar la modificacion.
    */
    public void vo_menuModificarUsuario() 
    {

        String str_dni;
        String str_nuevoNombre;
        String str_nuevoApellido;
        String str_nuevoDni;

        ArrayList<itfProperty> arrl_usuarios = 
        obj_gestorLN.arrl_listaUsuarios();

        if (arrl_usuarios.isEmpty()) 
        {
            System.out.println("No hay usuarios para modificar.");
            return;
        }

        System.out.println("Introduce el DNI del usuario a modificar:");
        str_dni = UtilidadesLP.leerCadena();

        boolean bln_usuarioExiste = false;
        
        for (itfProperty obj_auxUsuario : arrl_usuarios) 
        {
            
            if (obj_auxUsuario.getObjectProperty(clsConstantes.DNI_USUARIO).
            equals(str_dni)) 
            {
                bln_usuarioExiste = true;
                break;
            }
        }

        if (bln_usuarioExiste) 
        {
            System.out.println("Introduce el nuevo nombre:");
            str_nuevoNombre = UtilidadesLP.leerCadena();

            System.out.println("Introduce el nuevo apellido:");
            str_nuevoApellido = UtilidadesLP.leerCadena();

            System.out.println("Introduce el nuevo DNI:");
            str_nuevoDni = UtilidadesLP.leerCadena();

            obj_gestorLN.vo_modificarUsuario(str_nuevoNombre, str_nuevoApellido,
                    str_nuevoDni);
        } else 
        {
            System.out.println("No hay usuarios para modificar.");
        }
    }

   
    /**
    * Metodo publico que permite modificar un vehiculo.
    * Utiliza el objeto `obj_gestorLN` para realizar la modificacion.
    */
    private void vo_menuModificarVehiculo() 
    {
        System.out.println("Introduce la matrícula del vehículo a modificar:");
        String str_matricula = UtilidadesLP.leerCadena();
    
        clsVehiculo vehiculoAModificar = 
        obj_gestorLN.cls_buscarVehiculo(str_matricula);
    
        if (vehiculoAModificar != null) 
        {
            System.out.println("Introduce el nuevo color:");
            String str_nuevoColor = UtilidadesLP.leerCadena();
    
            System.out.println("Introduce el nuevo modelo:");
            String str_nuevoModelo = UtilidadesLP.leerCadena();
    
            System.out.println("Introduce los nuevos kilómetros:");
            int int_nuevosKilometros = UtilidadesLP.leerEntero();
    
            System.out.println("Introduce el nuevo motor:");
            String str_nuevoMotor = UtilidadesLP.leerCadena();
    
            obj_gestorLN.vo_modificarVehiculo(str_matricula, str_nuevoColor, 
            str_nuevoModelo,int_nuevosKilometros, str_nuevoMotor);
            
            System.out.println("Vehículo modificado con éxito.");
        } 
        else 
        {
            System.out.println("No hay vehículo con esa matrícula"+ 
            "para modificar.");
        }
    }

    /**
    * Metodo que permite dar de baja un vehiculo del sistema.
    * Muestra los vehiculos disponibles y solicita la matricula del vehiculo a
    * borrar.
    */
    private void vo_menuBajaVehiculo() 
    {

        vo_visualizarVehiculos();

        System.out.println("Dime la matricula del vehiculo a borrar: ");
        String mat = UtilidadesLP.leerCadena();
        obj_gestorLN.bln_borrarVehiculo( mat );

    }

    /**
    * Metodo que muestra la informacion de todos los coches ordenados por
    * matricula.
    * Recupera la informacion de los coches a traves del gestor y la muestra en
    * la consola.
    */
    private void vo_visualizarCochesPorMatricula() 
    {
     
    ArrayList<itfProperty> vehiculos = 
    obj_gestorLN.arrl_obtenerVehiculosPorMatricula();

    if (vehiculos.isEmpty()) 
    {
        System.out.println("No hay vehículos para mostrar");
    } 
    else 
    {
        System.out.println("=============VEHICULOS POR MATRICULA===========");
        for (itfProperty v : vehiculos) 
        {
            System.out.println(v.getObjectProperty(clsConstantes.MATRICULA));
        }
    }
    
    }

    /**
    * Metodo que muestra la informacion de todos los vehiculos disponibles en el
    * sistema.
    * Recupera la informacion de los vehiculos a traves del gestor y la muestra
    * en la consola.
    */
    private void vo_visualizarVehiculos() 
    {
        
        ArrayList<itfProperty> vehiculos = obj_gestorLN.arrl_obtenerVehiculos();
        if (vehiculos.isEmpty()) 
        {
            System.out.println("No hay vehículos para mostrar");
        } 
        else 
        {
            System.out.println("================VEHICULOS================:");
            for( itfProperty v : vehiculos )
            {
                System.out.println(v.getObjectProperty(clsConstantes.MATRICULA)+ 
                " " + v.getObjectProperty(clsConstantes.COLOR) + " " + 
                v.getObjectProperty(clsConstantes.MODELO) + " " + 
                v.getObjectProperty(clsConstantes.KILOMETRAJE) + " " + 
                v.getObjectProperty(clsConstantes.MOTOR));
            }
        }
    }

    /**
    * Metodo que muestra la informacion de todos los clientes ordenados por
    * direccion.
    * Recupera la informacion de los clientes a traves del gestor y la muestra
    * en la consola.
    */
    private void vo_visualizarClientesPorDireccion() 
    {
        ArrayList<itfProperty> clientes = 
        obj_gestorLN.arrl_obtenerClientesPorDireccion();

        if (clientes.isEmpty()) 
        {
            System.out.println("No hay clientes para mostrar");
        } else 
        {
            System.out.println("==========CLIENTES POR DIRECCION===========");
            for (itfProperty cliente : clientes) 
            {
                clsCliente clienteCast = (clsCliente) cliente;
                System.out.println("Nombre: " + clienteCast.getNombre());
            }
        }
    }

    /**
    * Metodo que muestra la informacion detallada de todos los clientes 
    * registrados en el sistema.
    * Recupera la informacion de los clientes a traves del gestor y la muestra
    * en la consola.
    */
    private void vo_visualizarClientes() 
    {
        
    
        ArrayList<itfProperty> usuarios = obj_gestorLN.arrl_recuperarUsuarios();

        if (usuarios.isEmpty()) 
        {
            System.out.println("No hay clientes para mostrar");
        } else 
        {
            System.out.println("=================CLIENTES=================");
            for (itfProperty u : usuarios) 
            {
                System.out.println(u.getObjectProperty(clsConstantes.DNI_USUARIO
                ) + " " + u.getObjectProperty(clsConstantes.APELLIDOS_USUARIO) +
                " " + u.getObjectProperty(clsConstantes.NOMBRE_USUARIO));
            }
        }
    } 

    /**
    * Metodo que permite dar de baja a un usuario del sistema.
    * Muestra los clientes disponibles y solicita el DNI del cliente a borrar.
    */
    private void vo_menuBajaUsuario() {
        
        System.out.println("Estos son los clientes: ");
        vo_visualizarClientes();

        System.out.println("Teclea dni del cliente a borrar: ");
        String dni = UtilidadesLP.leerCadena();

        obj_gestorLN.bln_borrarUsuario( dni );
    }

     /**
    * Metodo privado que muestra un menu para anadir un nuevo vehiculo.
    * Recolecta informacion del vehiculo y utiliza el objeto `obj_gestorLN` para
    * anadirlo.
    */
    private void vo_menuAñadirCoche() 
    {
        System.out.println("Teclea color: ");
        String color = UtilidadesLP.leerCadena();
        System.out.println("Teclea matricula: ");
        String matricula = UtilidadesLP.leerCadena();
        System.out.println("Teclea modelo: ");
        String modelo = UtilidadesLP.leerCadena();
        System.out.println("Teclea kilometros: ");
        int kms = UtilidadesLP.leerEntero();
        System.out.println("Teclea motor: ");
        String motor = UtilidadesLP.leerCadena();
        System.out.println("Teclea numero de puertas: ");
        int puertas = UtilidadesLP.leerEntero();
        System.out.println("Teclea asientos: ");
        int asientos = UtilidadesLP.leerEntero();
        System.out.println("Teclea tipo de cambio: ");
        String tipoCambio = UtilidadesLP.leerCadena();
        
        obj_gestorLN.vo_insertarCoche(color, matricula, modelo, kms, 
        motor, puertas, asientos, tipoCambio );
        
    }

}

