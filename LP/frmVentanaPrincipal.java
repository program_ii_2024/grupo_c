package LP;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import LN.clsGestorLN;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.border.LineBorder;

import COMUN.itfProperty;
import EXCEPCIONES.clsDNIFormatoIncorrecto;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JComboBox;

/**
 * Ventana principal de la aplicación.
 * 
 * Esta ventana sirve como la interfaz principal de la aplicación, ofreciendo 
 * diversas funcionalidades y opciones de gestión.
 * Implementa la interfaz ActionListener para manejar eventos de los componentes 
 * interactivos.
 */
public class frmVentanaPrincipal extends JFrame implements ActionListener {

	/**
	 * Panel de contenido principal de la ventana.
	 */
	private JPanel contentPane;

	/**
	 * Primer panel de la ventana.
	 */
	private JPanel panel;

	/**
	 * Segundo panel de la ventana.
	 */
	private JPanel panel_1;

	/**
	 * Tercer panel de la ventana.
	 */
	private JPanel panel_2;

	/**
	 * Área de desplazamiento para la lista de elementos.
	 */
	private JScrollPane scrollPane;

	/**
	 * Objeto de gestión de la lógica de negocio.
	 */
	private clsGestorLN obj_gln;

	/**
	 * Botón para borrar un elemento.
	 */
	private JButton btnBorrar;

	/**
	 * Botón para modificar un elemento.
	 */
	private JButton btnModificar;

	/**
	 * Botón para insertar un nuevo elemento.
	 */
	private JButton btnInsertar;

	/**
	 * Botón para ordenar por nota.
	 */
	private JButton btnOrdenNota;

	/**
	 * Botón para ordenar de manera natural.
	 */
	private JButton btnOrdenNatural;

	/**
	 * Lista de elementos a mostrar.
	 */
	private JList<itfProperty> list;

	/**
	 * Etiqueta para indicar la sección de inserción.
	 */
	private JLabel lblInsertar;

	/**
	 * Etiqueta para indicar la sección de borrado o modificación.
	 */
	private JLabel lblBorrarModificar;

	/**
	 * Etiqueta para indicar la sección de listado.
	 */
	private JLabel lblListado;

	/**
	 * Campo de texto para el nombre.
	 */
	private JTextField txtNombre;

	/**
	 * Campo de texto para el apellido.
	 */
	private JTextField txtApellido;

	/**
	 * Campo de texto para el rol.
	 */
	private JTextField txtRol1;

	/**
	 * Campo de texto para el DNI.
	 */
	private JTextField txtDNI;

	/**
	 * Campo de texto para el usuario.
	 */
	private JTextField txtUser;

	/**
	 * Campo de texto para la contraseña.
	 */
	private JTextField txtPassword;

	/**
	 * Campo de texto para la visa.
	 */
	private JTextField txtVisa;

	/**
	 * Lista de elementos de tipo itfProperty.
	 */
	private ArrayList<itfProperty> arrl_estuds;

	/**
	 * Modelo de lista predeterminado para los elementos.
	 */
	private DefaultListModel<itfProperty> dlm;

	/**
	 * Contador.
	 */
	private int int_cont = 0;

	/**
	 * Booleano para indicar si avanza o no.
	 */
	private boolean bln_avanza = true;

	/**
	 * Hilo utilizado para operaciones visuales en la ventana.
	 */
	private Thread hilo = new Hilo();

	/**
	 * Modelo de lista predeterminado para los elementos de la lista desplegable
	 */
	private DefaultComboBoxModel<String> dcbmDNIs;

	/**
	 * Lista desplegable para mostrar los elementos.
	 */
	private JComboBox<String> comboBox;

	/**
	 * Etiqueta para indicar un campo específico.
	 */
	private JLabel lblNewLabel_7;

	/**
	 * Etiqueta para indicar un campo específico.
	 */
	private JLabel lblNewLabel_8;

	/**
	 * Etiqueta para indicar un campo específico.
	 */
	private JLabel lblNewLabel_9;

	/**
	 * Etiqueta para indicar un campo específico.
	 */
	private JLabel lblNewLabel_10;

	/**
	 * Etiqueta para indicar un campo específico.
	 */
	private JLabel lblNewLabel_11;

	/**
	 * Campo de texto para el rol en la sección de borrado.
	 */
	private JTextField txtRolBorrar;

	/**
	 * Campo de texto para el nombre en la sección de borrado.
	 */
	private JTextField txtNombreBorrar;

	/**
	 * Campo de texto para el apellido en la sección de borrado.
	 */
	private JTextField txtApellidoBorrar;

	/**
	 * Campo de texto para el usuario en la sección de borrado.
	 */
	private JTextField txtUserBorrar;

	/**
	 * Campo de texto para la contraseña en la sección de borrado.
	 */
	private JTextField txtPasswordBorrar;

	/**
	 * Campo de texto para el email.
	 */
	private JTextField txtEmail;

	/**
	 * Campo de texto para la dirección.
	 */
	private JTextField txtDireccion;


	/**
	 * Ventana principal de la aplicación que permite la gestión de usuarios.
	 * 
	 * Esta ventana muestra un formulario para insertar nuevos usuarios, así 
	 * como opciones para borrar, modificar y visualizar usuarios existentes.
	 * Además, presenta un listado de usuarios que se puede ordenar por nota o 
	 * en orden natural.
	 * 
	 * @param obj_gln El gestor de la lógica de negocio utilizado para 
	 * interactuar con la base de datos y manejar la información de los usuarios
	 * @param int_rol El rol del usuario actual, determina que acciones puede 
	 * realizar en la ventana.
	 */
	public frmVentanaPrincipal(clsGestorLN obj_gln, int int_rol) {

		JOptionPane.showMessageDialog(null, "Tienes rol " + 
										int_rol);

		this.obj_gln = obj_gln;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1067, 595);
		contentPane = new JPanel();
		contentPane.setBorder(new 
							EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);

		panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(10, 72, 326, 474);
		contentPane.add(panel);
		panel.setLayout(null);

		btnInsertar = new JButton("Insertar");

		btnInsertar.setBounds(89, 386, 129, 54);
		panel.add(btnInsertar);

		JLabel lblNewLabel = new JLabel("Nombre:");
		lblNewLabel.setBounds(30, 67, 111, 14);
		panel.add(lblNewLabel);

		txtNombre = new JTextField();
		txtNombre.setBounds(151, 64, 154, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);

		txtApellido = new JTextField();
		txtApellido.setBounds(151, 95, 129, 20);
		panel.add(txtApellido);
		txtApellido.setColumns(10);

		txtRol1 = new JTextField();
		txtRol1.setBounds(151, 126, 59, 20);
		panel.add(txtRol1);
		txtRol1.setColumns(10);

		txtDNI = new JTextField();
		txtDNI.setBounds(151, 32, 86, 20);
		panel.add(txtDNI);
		txtDNI.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Apellido:");
		lblNewLabel_1.setBounds(29, 98, 112, 14);
		panel.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Rol:");
		lblNewLabel_2.setBounds(30, 129, 111, 14);
		panel.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("DNI:");
		lblNewLabel_3.setBounds(29, 35, 112, 14);
		panel.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("User:");
		lblNewLabel_4.setBounds(30, 160, 111, 14);
		panel.add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel("Password:");
		lblNewLabel_5.setBounds(30, 197, 111, 14);
		panel.add(lblNewLabel_5);

		JLabel lblNewLabel_6 = new JLabel("VISA:");
		lblNewLabel_6.setBounds(29, 235, 112, 14);
		panel.add(lblNewLabel_6);

		txtUser = new JTextField();
		txtUser.setBounds(151, 157, 55, 20);
		panel.add(txtUser);
		txtUser.setColumns(10);

		txtPassword = new JTextField();
		txtPassword.setBounds(151, 194, 111, 20);
		panel.add(txtPassword);
		txtPassword.setColumns(10);

		txtVisa = new JTextField();
		txtVisa.setBounds(151, 232, 129, 20);
		panel.add(txtVisa);
		txtVisa.setColumns(10);
		
		JLabel lblNewLabel_12 = new JLabel("Email:");
		lblNewLabel_12.setBounds(30, 278, 82, 13);
		panel.add(lblNewLabel_12);
		
		JLabel lblNewLabel_13 = new JLabel("Direccion:");
		lblNewLabel_13.setBounds(30, 314, 82, 13);
		panel.add(lblNewLabel_13);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(141, 275, 139, 19);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtDireccion = new JTextField();
		txtDireccion.setBounds(122, 311, 158, 19);
		panel.add(txtDireccion);
		txtDireccion.setColumns(10);

		panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.setBounds(343, 72, 332, 474);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		btnBorrar = new JButton("Borrar");
	
		btnBorrar.setBounds(10, 440, 89, 23);
		panel_1.add(btnBorrar);

		btnModificar = new JButton("Modificar");
		btnModificar.setBounds(233, 440, 89, 23);
		panel_1.add(btnModificar);

		dcbmDNIs = new DefaultComboBoxModel<>();
		comboBox = new JComboBox<String>();
		comboBox.setModel(dcbmDNIs);
		comboBox.setBounds(87, 82, 153, 22);
		panel_1.add(comboBox);

		txtNombreBorrar = new JTextField();
		txtNombreBorrar.setColumns(10);
		txtNombreBorrar.setBounds(150, 126, 154, 20);
		panel_1.add(txtNombreBorrar);

		lblNewLabel_7 = new JLabel("Nombre:");
		lblNewLabel_7.setBounds(29, 129, 111, 14);
		panel_1.add(lblNewLabel_7);

		lblNewLabel_8 = new JLabel("Apellido:");
		lblNewLabel_8.setBounds(28, 160, 112, 14);
		panel_1.add(lblNewLabel_8);

		txtApellidoBorrar = new JTextField();
		txtApellidoBorrar.setColumns(10);
		txtApellidoBorrar.setBounds(150, 157, 49, 20);
		panel_1.add(txtApellidoBorrar);

		txtRolBorrar = new JTextField();
		txtRolBorrar.setColumns(10);
		txtRolBorrar.setBounds(150, 188, 29, 20);
		panel_1.add(txtRolBorrar);

		lblNewLabel_9 = new JLabel("Rol:");
		lblNewLabel_9.setBounds(29, 191, 111, 14);
		panel_1.add(lblNewLabel_9);

		lblNewLabel_10 = new JLabel("User:");
		lblNewLabel_10.setBounds(29, 222, 111, 14);
		panel_1.add(lblNewLabel_10);

		txtUserBorrar = new JTextField();

		txtUserBorrar.setColumns(10);
		txtUserBorrar.setBounds(150, 219, 55, 20);
		panel_1.add(txtUserBorrar);

		txtPasswordBorrar = new JTextField();
		txtPasswordBorrar.setColumns(10);
		txtPasswordBorrar.setBounds(150, 256, 111, 20);
		panel_1.add(txtPasswordBorrar);

		lblNewLabel_11 = new JLabel("Password:");
		lblNewLabel_11.setBounds(29, 259, 111, 14);
		panel_1.add(lblNewLabel_11);

		panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_2.setBounds(685, 72, 356, 474);
		contentPane.add(panel_2);
		panel_2.setLayout(null);

		btnOrdenNota = new JButton("Ordenados por nota");
		
		btnOrdenNota.setBounds(10, 440, 129, 23);
		panel_2.add(btnOrdenNota);

		btnOrdenNatural = new JButton("Orden natural");
		btnOrdenNatural.setBounds(257, 440, 89, 23);
		panel_2.add(btnOrdenNatural);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(59, 35, 245, 381);
		panel_2.add(scrollPane);

		list = new JList<>();
		scrollPane.setViewportView(list);

		lblInsertar = new JLabel("Insertar");
		lblInsertar.setBounds(146, 35, 46, 14);
		contentPane.add(lblInsertar);

		lblBorrarModificar = new JLabel("Borrar/Modificar");
		lblBorrarModificar.setBounds(470, 35, 100, 14);
		contentPane.add(lblBorrarModificar);

		lblListado = new JLabel("Listado");
		lblListado.setBounds(847, 35, 78, 14);
		contentPane.add(lblListado);

		setLocationRelativeTo(null);
		setVisible(true);

		// ------------------- CONTROL DE ACCESO ---------------------

		if (int_rol == 1) {
			btnInsertar.setEnabled(false);
			btnBorrar.setEnabled(false);
			btnModificar.setEnabled(false);
		}
		// ADMIN
		else if (int_rol == 2) {
			btnInsertar.setEnabled(true);
			btnBorrar.setEnabled(true);
			btnModificar.setEnabled(true);
		} else {
			btnInsertar.setEnabled(true);
			btnBorrar.setEnabled(false);
			btnModificar.setEnabled(false);
		}

		btnInsertar.addActionListener(this);
		btnBorrar.addActionListener(this);
		btnModificar.addActionListener(this);
		btnOrdenNatural.addActionListener(this);
		btnOrdenNota.addActionListener(this);

		comboBox.addActionListener(this);

		dlm = new DefaultListModel<>();
		list.setModel(dlm);

		// esto muestra los estudiantes a la derecha
		arrl_estuds = obj_gln.arrl_recuperarUsuarios();
		dlm.addAll(arrl_estuds);

		// mostrar los dni en la combobox para borrar

		ArrayList<String> dnis = obj_gln.arrl_recuperarDNIsClientes();
		dcbmDNIs.addAll(dnis);

		System.out.println( dnis );

		hilo.start();

	}

	// -------------------- TODOS TUS CLICKS ------------------------------

	/**
	 * Maneja las acciones realizadas en la ventana.
	 * 
	 * @param e El evento de acción que desencadenó el método.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

		switch (e.getActionCommand()) {
			case "Insertar":
				vo_procesoInsertar();
				break;

			case "Borrar":
				vo_procesoBorrar();
				break;

			case "Modificar":
				vo_procesoModificar();
				break;

			case "Ordenados por nota":
				vo_procesoOrdenarNota();
				break;

			case "Orden natural":
				vo_procesoOrdenarDNI();
				break;

			case "comboBoxChanged":
				vo_procesoCargarDatosFormulario();
		}
	}

	// *****************************************************************/

	private void vo_procesoCargarDatosFormulario() 
	{

		String dni = (String) comboBox.getSelectedItem();

		String nombre = obj_gln.str_recuperarNombreCliente(dni);
		String apellido = obj_gln.str_recuperarApellidoCliente(dni);
		int rol = obj_gln.int_recuperarRolCliente(dni);
		String user = obj_gln.str_recuperarUserCliente(dni);
		String pass = obj_gln.str_recuperarPassCliente(dni);

		txtNombreBorrar.setText( nombre );
		txtRolBorrar.setText("" +rol);
		txtApellidoBorrar.setText(apellido);
		txtUserBorrar.setText(user);
		txtPasswordBorrar.setText(pass);

	}

	private void vo_procesoModificar() 
	{
		JOptionPane.showMessageDialog(
					null, "MODIFICAR DATOS CLIENTE");

		String nombre = txtNombreBorrar.getText();
		String apellido = txtApellidoBorrar.getText();
		int rol = Integer.parseInt(txtRolBorrar.getText());
		String user = txtUserBorrar.getText();
		String pass = txtPasswordBorrar.getText();

		// el dni que has seleccionado de la combobox
		String dni = (String) comboBox.getSelectedItem();

		// llamar a modificar
		obj_gln.vo_modificarCliente(dni, nombre, apellido, rol, user, pass);

		// actualiza los datos de la lista de la derecha
		vo_procesoOrdenarDNI();
	}

	private void vo_procesoOrdenarDNI() 
	{

		List<itfProperty> r = obj_gln.arrl_getClientesOrdenadosDNI();

		// borrar
		dlm.clear();
		// actualizar
		dlm.addAll(r);
	}

	private void vo_procesoOrdenarNota() 
	{
		// 1.Recuperar Ordenados por el criterio
		List<itfProperty> r = obj_gln.list_getClientesOrdenadosApellido();

		// 2.Limpiar la lista
		dlm.clear();

		// 3.Mostrar el nuevo orden
		dlm.addAll(r);
	}

	private void vo_procesoBorrar() 
	{
		String str_dni = (String) comboBox.getSelectedItem();
		JOptionPane.showMessageDialog(null, "BORRAR el dni :" 
										+ str_dni);
		obj_gln.bln_borrarUsuario(str_dni);
		vo_procesoOrdenarDNI();
		dcbmDNIs.removeAllElements();
		obj_gln.arrl_dniClientes.remove(str_dni);
		ArrayList<String> dnis = obj_gln.arrl_recuperarDNIsClientes();
		dcbmDNIs.addAll(dnis);

		dlm.clear();
		arrl_estuds = obj_gln.arrl_getClientesOrdenadosDNI();

		System.out.println( arrl_estuds );

		dlm.addAll(arrl_estuds);
	}

	private void vo_procesoInsertar() 
	{

		if (txtDNI.getText().length() == 0 ||
				txtNombre.getText().length() == 0 ||
				txtApellido.getText().length() == 0
		// ...
		) {
			JOptionPane.showMessageDialog(null, "Debes rellenar todos los datos!");
		} else {
			try 
			{

				String str_dni = txtDNI.getText();
				String str_nombre = txtNombre.getText();
				//double nota = Double.parseDouble(txtNota.getText());
				int rol = Integer.parseInt(txtRol1.getText());
				String user = txtUser.getText();
				String pass = txtPassword.getText();
				String apellido = txtApellido.getText();
				String email = txtEmail.getText();
				String str_direccion = txtDireccion.getText();
				String numVisa = txtVisa.getText();

				this.obj_gln.vo_insertarCliente(str_nombre, apellido, 
				str_dni, email, 611493822, rol, pass, user,
				 str_direccion, numVisa);

				// refrescar la lista de la derecha
				dlm.clear();
				arrl_estuds = obj_gln.arrl_getClientesOrdenadosDNI();
				dlm.addAll(arrl_estuds);

				// refrescar combobox
				dcbmDNIs.removeAllElements();
				List<String> dnis = obj_gln.arrl_recuperarDNIsClientes();
				dcbmDNIs.addAll(dnis);

				JOptionPane.showMessageDialog(null, "Insertado correctamente!");

			} catch (clsDNIFormatoIncorrecto ex) {
				JOptionPane.showMessageDialog(null, "El dni es incorrecto!");
			}
		}
	}
}
