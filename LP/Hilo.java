package LP;

import javax.swing.JOptionPane;

/**
 * Clase que representa el hilo de ejecución.
 */
public class Hilo extends Thread {

    private int minutos = 0;

    /**
     * Método que inicia la ejecución del hilo.
     */
    @Override
    public void run() {

        while (true) {
            try {
                Thread.sleep( 60 * 1000);

                minutos++;
              
                JOptionPane.showMessageDialog(null, 
                "Llevas " + minutos + " minutos conectado");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
