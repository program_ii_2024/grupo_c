package LP;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import LN.clsGestorLN;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Diálogo de ventana de inicio de sesión que permite a los usuarios ingresar al sistema.
 * Implementa la interfaz ActionListener para manejar eventos de los botones.
 */
//reemplaza a public class clsMenu
public class dlgVentanaLogin extends JDialog implements ActionListener 
{

	//****************** INICIO COMPONENTES ***************************
	/**
	 * Panel principal que contiene los componentes de la ventana.
	 */
	private JPanel contentPanel = new JPanel();
	/**
	 * Panel que contiene los botones de la ventana.
	 */
	private JPanel buttonPane = new JPanel();

	//private JButton btnRegistrar = new JButton("Registrar");
	/**
	 * Botón para aceptar la entrada del usuario.
	 */
	private JButton btnAceptar = new JButton("Aceptar");
	/**
	 * Botón para cancelar el inicio de sesión.
	 */
	private JButton btnCancelar = new JButton("Cancelar");

	/**
	 * Etiqueta para mostrar "Usuario:".
	 */
	private JLabel lblUsuario = new JLabel("Usuario:");
	/**
	 * Etiqueta para mostrar "Contraseña:".
	 */
	private JLabel lblPassword = new JLabel("Contraseña:");
	/**
	 * Etiqueta para mostrar una foto relacionada con el inicio de sesión.
	 */
	private JLabel lblFoto = new JLabel("Foto");

	/**
	 * Campo de texto para ingresar el nombre de usuario.
	 */
	private JTextField txtUsuario;
	/**
	 * Campo de contraseña para ingresar la contraseña del usuario.
	 */
	private JPasswordField pwdPassword;
	//****************** FIN COMPONENTES ***************************

	//*********************** GESTOR LN **********************
	/**
	 * Objeto gestor de lógica de negocio.
	 */
	private clsGestorLN obj_gln;
	//*********************** GESTOR LN **********************
	
	//***********************CONSTRUCTOR DE LA VENTANA*****************

	/**
	 * Constructor de la ventana de inicio de sesion.
	 * Inicializa los componentes de la ventana y establece su diseño y 
	 * disposicion.
	 * Carga una imagen de un recurso para la etiqueta de foto.
	 */
	public dlgVentanaLogin() 
	{

		obj_gln = new clsGestorLN(); //<--------- !!!

		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new 
								EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		lblUsuario.setBounds(148, 42, 104, 14);
		contentPanel.add(lblUsuario);
		lblPassword.setBounds(148, 92, 104, 14);
		contentPanel.add(lblPassword);
		txtUsuario = new JTextField();
		txtUsuario.setBounds(222, 39, 86, 20);
		contentPanel.add(txtUsuario);
		txtUsuario.setColumns(10);
		lblFoto.setBounds(28, 35, 100, 100);
		contentPanel.add(lblFoto);
		pwdPassword = new JPasswordField();
		pwdPassword.setBounds(225, 92, 83, 20);
		contentPanel.add(pwdPassword);
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		//buttonPane.add(btnRegistrar);
		buttonPane.add(btnAceptar);
		getRootPane().setDefaultButton(btnAceptar);
		buttonPane.add(btnCancelar);

		//crear una carpeta resources
		//Si 'resources' está en el classpath y 'img' está dentro de 'resources'
		ImageIcon icon = new ImageIcon(getClass().
											getResource("/img/login.png"));
		lblFoto.setIcon( icon );

		//--------------------------------------------------------------
		
		btnAceptar.setActionCommand("Aceptar");
		btnCancelar.setActionCommand("Cancelar");
		//this.btnRegistrar.setActionCommand("Registrar");

		//btnRegistrar.addActionListener(this);
		btnAceptar.addActionListener(this);
		btnCancelar.addActionListener(this);

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(false);
		setTitle("Gestion Talleres");
		setBounds(100, 100, 390, 205);
		setLocationRelativeTo(null);
		setVisible(true);

		//--------------------------------------------------------------
	}

	//*********************** FIN CONSTRUCTOR ****************************

	//*************************** ESCUCHADOR *************************
	/**
	 * Maneja las acciones realizadas en la ventana de inicio de sesión.
	 * 
	 * @param e El evento de acción que desencadenó el método.
	 */
	public void actionPerformed(ActionEvent e) 
	{

		switch (e.getActionCommand()) 
		{
			
			case "Aceptar":

				System.out.println("Ok!");

				String user = txtUsuario.getText();
				String pass = new String( pwdPassword.getPassword() );

				//necesitamos usar el password
				int rol = obj_gln.int_obtenerCredencialesUserPass(user , pass);

				//usuario registrado???
				if ( rol == 0 )
				{
					setVisible(false);
					new frmVentanaPrincipal(obj_gln , rol );//muestra la ventana
				}
				//usuario invitado???
				else if ( rol == 1 )
				{
					setVisible(false);
					new frmVentanaPrincipal(obj_gln , rol );
				}
				//usuario admin???
				else if ( rol == 2 )
				{
					setVisible(false);
					new frmVentanaPrincipal(obj_gln , rol );
				}				
				else
				{
					JOptionPane.showMessageDialog(null, "Ese usuario no existe!!");
					txtUsuario.setText("");
					pwdPassword.setText("");
					txtUsuario.requestFocus();
				}
				//1 ventana 3 paneles si eres admin acceso/visible a los 3
				//insertar / borrar / consultar
				break;

			case "Cancelar":
				System.out.println("Cancelar!");
				System.exit(0);
				break;
			
			case "Registrar":
				System.out.println("DEBUG: Registrar!");
				
				//llamar a la ventana de registro enviando su gln
				new dlgVentanaRegistro( obj_gln ); 
				break;
		}
		
	}

}
