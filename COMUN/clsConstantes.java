package COMUN;

/**
 * Clase que define los atributos utilizados en el resto de clases del programa.
 * 
 * @author unai.pinilla
 * @version 4.0
 */
public class clsConstantes {

    /**
     * Atributos de la clase clsUsuario.
     */
    public static final String NOMBRE_USUARIO = "Nombre del usuario";
    /** Apellidos del usuario. */
    public static final String APELLIDOS_USUARIO = "Apellidos del usuario";
    /** DNI del usuario. */
    public static final String DNI_USUARIO = "DNI del usuario";
    /** Email del usuario. */
    public static final String EMAIL_USUARIO = "Email del usuario";
    /** Telefono del usuario. */
    public static final String TELEFONO_USUARIO = "Telefono del usuario";
    /** Contraseña del usuario. */
    public static final String CONTRASEÑA_USUARIO = "Contraseña del usuario";
    /** Nombre de usuario del usuario. */
    public static final String NOMBREUSUARIO_USUARIO = "Nombre de usuario del"
                                                        + " usuario";

    /**
     * Atributos de la clase clsMecanico.
     */
    /** Puesto del mecanico. */
    public static final String PUESTO = "Puesto del mecanico";
    /** Sueldo del mecanico. */
    public static final String SUELDO = "Sueldo del mecanico";

    /**
     * Atributos de la clase clsGerente.
     */
    /** Beneficio del gerente. */
    public static final String BENEFICIO = "Beneficio del gerente";
    /** Inversion del gerente. */
    public static final String INVERSION = "Inversion del gerente";
    /** Propiedades del gerente. */
    public static final String PROPIEDADES = "Propiedades del gerente";

    /**
     * Atributos de la clase clsCliente.
     */
    /** Direccion del cliente. */
    public static final String DIRECCION = "Direccion del cliente";
    /** Numero de VISA del cliente. */
    public static final String NUM_VISA = "Numero de VISA del cliente";

    /**
     * Atributos de la clase clsVehiculo.
     */
    /** Color del coche. */
    public static final String COLOR = "Color del coche";
    /** Matricula del coche. */
    public static final String MATRICULA = "Matricula del coche";
    /** Modelo del coche. */
    public static final String MODELO = "Modelo del coche";
    /** Kilometraje del coche. */
    public static final String KILOMETRAJE = "Kilometraje del coche";
    /** Motor del coche. */
    public static final String MOTOR = "Motor del coche";

    /**
     * Atributos de la clase clsCoche.
     */
    /** Numero de puertas del coche. */
    public static final String NUMERO_PUERTAS = "Numero de puertas del coche";
    /** Numero de asientos del coche. */
    public static final String NUMERO_ASIENTOS = "Numero de asientos del coche";
    /** Tipo de cambio del coche. */
    public static final String TIPO_CAMBIO = "Tipo de cambio del coche";

    /**
     * Atributos de la clase clsMotocicleta.
     */
    /** Numero de ruedas de la motocicleta. */
    public static final String NUMERO_RUEDAS = "Numero de ruedas de la" +
                                                " motocicleta";
    /** Cilindrada de la motocicleta. */
    public static final String CILINDRADA = "Cilindrada de la motocicleta";

    
}
