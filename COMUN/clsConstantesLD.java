package COMUN;

/**
 * Clase que contiene las constantes SQL para las operaciones CRUD
 * (Crear, Leer, Actualizar, Borrar) sobre las tablas de cliente,
 * gerente y mecánico en la base de datos del taller mecánico.
 * 
 * Cada constante representa una consulta SQL específica para realizar
 * una operación particular en la base de datos.
 */
public class clsConstantesLD {

    /**
     * Consulta SQL para insertar un cliente en la base de datos.
     */
    public static final String SQL_INSERT_CLIENTE = 
    "INSERT INTO `taller_mecanico`.`cliente` (`nombre`, `apellido`, `dni`," +  
    "`email`, `telefono`, `rol`, `direccion`, `numVisa`, `contraseña`,"+ 
    "`nombreUsuario`) VALUES (?,?,?,?,?,?,?,?,?,?)";

    /**
     * Consulta SQL para selecionar un cliente en la base de datos.
     */
    public static final String SQL_SELECT_CLIENTE = "SELECT * FROM taller_" + 
    "mecanico.cliente;";
    /**
     * Consulta SQL para borrar un cliente en la base de datos.
     */
    public static final String SQL_DELETE_CLIENTE = 
    "DELETE FROM `taller_mecanico`.`cliente` WHERE (`dni` = ?);";
    
    /**
     * Consulta SQL para actualizar un cliente en la base de datos.
     */
    public static final String SQL_UPDATE_CLIENTE = 
    "UPDATE `taller_mecanico`.`cliente` SET `nombre` = ?,`apellido` = ?, `rol`"+
    " = ?,`contraseña` = ?, `nombreUsuario` = ? WHERE (`dni` = ?);";

    /**
     * Consulta SQL para insertar un gerente en la base de datos.
     */
    public static final String SQL_INSERT_GERENTE = "";
    /**
     * Consulta SQL para selecionar un gerente en la base de datos.
     */
    public static final String SQL_SELECT_GERENTE = "";
    /**
     * Consulta SQL para borrar un gerente en la base de datos.
     */
    public static final String SQL_DELETE_GERENTE = "";
    /**
     * Consulta SQL para actualizar un gerente en la base de datos.
     */
    public static final String SQL_UPDATE_GERENTE = "";

    /**
     * Consulta SQL para insertar un mecanico en la base de datos.
     */
    public static final String SQL_INSERT_MECANICO = "";
    /**
     * Consulta SQL para selecionar un mecanico en la base de datos.
     */
    public static final String SQL_SELECT_MECANICO = "";
    /**
     * Consulta SQL para borrar un mecanico en la base de datos.
     */
    public static final String SQL_DELETE_MECANICO = "";
    /**
     * Consulta SQL para actualizar un mecanico en la base de datos.
     */
    public static final String SQL_UPDATE_MECANICO = "";
     
}
