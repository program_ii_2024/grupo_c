package COMUN;

import LP.dlgVentanaLogin;

/**
 * Clase principal del programa. Contiene el metodo main el cual inicializara 
 * el programa. 
 * 
 * @author u.garay
 * @version 3.0
 */
public class clsMain 
{

    /**
     * Metodo principal que inicia la aplicacion. Llama al metodo vo_menu_inicio
     * para que se muestre el menu tras crear una instancia de la clase clsMenu.
     * 
     * @author u.garay
     * 
     * @param args Argumentos de linea de comandos.
     */
    public static void main(String[] args) 
    {

        //clsMenu objMenu;
        //objMenu = new clsMenu();
        //objMenu.vo_menuInicio();

        new dlgVentanaLogin();
        
    }
}