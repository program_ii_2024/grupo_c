package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Clase jmenu que extiende JFrame e implementa ActionListener para crear un 
 * menu con opciones de tamaño de ventana y color de fondo.
 */
public class jmenu extends JFrame implements ActionListener
{

    /**
     * La barra de menú principal del JFrame.
     */
    private JMenuBar menuBar;

    /**
     * El primer menú en la barra de menú, etiquetado como "Opciones".
     */
    private JMenu menu1;

    /**
     * El segundo menú en la barra de menú, etiquetado como "Tamaño de la ventana".
     */
    private JMenu menu2;

    /**
     * El tercer menú en la barra de menú, etiquetado como "Color de fondo".
     */
    private JMenu menu3;

    /**
     * El primer ítem del menú que establece el tamaño de la ventana a 640x480.
     */
    private JMenuItem menuItem21;

    /**
     * El segundo ítem del menú que establece el tamaño de la ventana a 1024x768.
     */
    private JMenuItem menuItem22;

    /**
     * El primer ítem del menú que cambia el color de fondo a rojo.
     */
    private JMenuItem menuItem31;

    /**
     * El segundo ítem del menú que cambia el color de fondo a verde.
     */
    private JMenuItem menuItem32;

    /**
     * Constructor de la clase jmenu que inicializa y configura el menu y el 
     * JFrame.
     */
    public jmenu() 
    {

        /* Creamos el JMenuBar y lo asociamos con el JFrame */
        menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        /* Creamos el primer JMenu y lo pasamos como parámetro al JMenuBar 
        mediante el método add */
        menu1 = new JMenu("Opciones");
        menuBar.add(menu1);

        /* Creamos el segundo y tercer objetos de la clase JMenu y los asociamos
        con el primer JMenu creado */
        menu2 = new JMenu("Tamaño de la ventana");
        menu1.add(menu2);
        menu3 = new JMenu("Color de fondo");
        menu1.add(menu3);

        /* Creamos los dos primeros objetos de la clase JMenuItem y los 
        asociamos con el segundo JMenu */
        menuItem21 = new JMenuItem("640*480");
        menu2.add(menuItem21);
        menuItem21.addActionListener(this);
        menuItem22 = new JMenuItem("1024*768");
        menu2.add(menuItem22);
        menuItem22.addActionListener(this);

        /* Creamos los otros dos objetos de la clase JMenuItem y los  asociamos 
        con el tercer JMenu */
        menuItem31 = new JMenuItem("Rojo");
        menu3.add(menuItem31);
        menuItem31.addActionListener(this);
        menuItem32 = new JMenuItem("Verde");
        menu3.add(menuItem32);
        menuItem32.addActionListener(this);

        //COnfigurar y mostrar JFrame
        initPantalla();
    }

    /**
     * Metodo que inicializa la configuracion del JFrame.
     */
    private void initPantalla() 
    {
        setLayout(null); //Layout absoluto
        setTitle("JMenu"); //Título del JFrame
        setSize(300, 200); //Dimensiones del JFrame
        setResizable(false); //No redimensionable
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Cerrar proceso 
        setVisible(true); //Mostrar JFrame
    }

    /**
     * Método que implementa las acciones de cada ítem de menú.
     * 
     * @param e El evento de acción.
     */
    public void actionPerformed(ActionEvent e) 
    {
        if (e.getSource() == menuItem21) 
        {
            setSize(640,480);
        }
        if (e.getSource() == menuItem22) 
        {
            setSize(1024,768);
        }
        if (e.getSource() == menuItem31) 
        {
            getContentPane().setBackground(new Color(255,0,0));
        }
        if (e.getSource() == menuItem32) 
        {
            getContentPane().setBackground(new Color(0,255,0));
        }
    }

    /**
     * Método principal que crea una instancia de la clase jmenu.
     * 
     * @param args Los argumentos de la línea de comandos.
     */
    public static void main(String[] args) 
    {

        new jmenu();

    }

}