package EXCEPCIONES;

/**
 * Excepcion lanzada cuando se detecta un formato incorrecto en un numero de 
 * DNI.
 * 
 * Esta excepcion extiende la clase Exception y se utiliza para indicar que un 
 * numero de DNI no cumple con el formato esperado.
 */
public class clsDNIFormatoIncorrecto extends Exception
{

    /**
     * Constructor de la clase clsDNIFormatoIncorrecto.
     * 
     * @param mensaje Mensaje descriptivo que explica el motivo de la excepcion.
     */
    public clsDNIFormatoIncorrecto( String mensaje )
    {
        super( mensaje );
    }

}
