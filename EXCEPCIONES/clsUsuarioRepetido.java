package EXCEPCIONES;

/**
 * Excepcion lanzada cuando se intenta agregar un estudiante que ya esta 
 * registrado.
 * 
 * Esta excepcion extiende la clase Exception y se utiliza para indicar que se 
 * esta intentando agregar un estudiante que ya se encuentra en la lista de 
 * estudiantes.
 */
public class clsUsuarioRepetido extends Exception 
{

     /**
     * Constructor sin argumentos de la clase clsEstudianteRepetido.
     */
    public clsUsuarioRepetido()
    {
        super();
    }

    /**
     * Constructor de la clase clsEstudianteRepetido.
     * 
     * @param mensaje Mensaje descriptivo que explica el motivo de la excepcion.
     */
    public clsUsuarioRepetido( String mensaje )
    {
        super( mensaje );
    }

}
