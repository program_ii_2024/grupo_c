package EXCEPCIONES;

/**
 * Excepcion lanzada cuando se intenta acceder a una propiedad que no existe.
 * 
 * Esta excepcion extiende la clase RuntimeException y se utiliza para indicar 
 * que se esta intentando acceder a una propiedad que no existe en una 
 * estructura de datos o clase.
 */
public class clsPropiedadNoExistente extends RuntimeException
{

    /**
     * Constructor de la clase clsPropiedadNoExistente.
     * 
     * @param mensaje Mensaje descriptivo que explica el motivo de la excepcion.
     */
    public clsPropiedadNoExistente(String mensaje ) 
    {
       super( mensaje );
    }
    
}
