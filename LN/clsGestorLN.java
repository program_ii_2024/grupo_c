package LN;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import COMUN.itfProperty;
import EXCEPCIONES.clsDNIFormatoIncorrecto;
import EXCEPCIONES.clsUsuarioRepetido;
import LD.clsGestorDatos;

/**
 * Clase que gestiona la lista de usuarios, permitiendo dar de alta,
 * dar de baja y modificar datos de los mismos.
 * 
 * Esta clase utiliza un conjunto para almacenar usuarios y un mapa para
 * almacenar vehiculos.
 * 
 * @author unai.pinilla
 * @version 4.00
 */
public class clsGestorLN {
    /**
     * Conjunto que almacena los clientes registrados en el sistema.
     * 
     * Este conjunto garantiza que no haya clientes duplicados y mantiene un
     * orden determinado para los elementos. Los clientes son objetos de la
     * clase clsCliente.
     * 
     * @see clsCliente
     */
    private Set<clsCliente> set_usuariosClientes;

    /**
     * Conjunto que almacena los mecánicos registrados en el sistema.
     * 
     * Este conjunto garantiza que no haya mecánicos duplicados y mantiene un
     * orden determinado para los elementos. Los mecánicos son objetos de la
     * clase clsMecanico.
     * 
     * @see clsMecanico
     */
    private Set<clsMecanico> set_usuariosMecanicos;

    /**
     * Conjunto que almacena los usuarios registrados en el sistema.
     * 
     * Este conjunto garantiza que no haya usuarios duplicados y mantiene un
     * orden determinado para los elementos. Los usuarios son objetos de la
     * clase clsUsuario.
     * 
     * @see clsUsuario
     */
    private Set<clsUsuario> set_usuarios;

    /**
     * Mapa que almacena los vehículos registrados en el sistema.
     * 
     * Este mapa utiliza la matrícula del vehículo como clave y el objeto
     * clsVehiculo como valor. Esto permite una rápida búsqueda de vehículos
     * por su matrícula.
     * 
     * @see clsVehiculo
     */
    private Map<String, clsVehiculo> map_vehiculos;

    /**
     * Listas que almacenaran los dni de los usuarios cuando se crean un
     * usuario, los dni de los clientes que se añaden desde los menus de gerente
     * o de mecanico y los dni de los mecanicos que el gerente añada.
     * 
     * Serviran para que un metodo pueda verificar que, al intentar acceder a
     * los respectivos menus, el usuario ha sido añadido y se ha registrado.
     */
    public ArrayList<String> arrl_dniRegistrados = new ArrayList<>();
    /** Lista de los DNI de los clientes  */
    public ArrayList<String> arrl_dniClientes = new ArrayList<>();
    /** Lista de los DNI de los mecanicos  */
    public ArrayList<String> arrl_dniMecanicos = new ArrayList<>();

    /**
     * Mapas que almacena las contraseñas y los nombres de usuario que se
     * inicializan en el menu.
     * Ambos usaran el dni del usuario como clave.
     */
    public Map<String, String> map_contraseñas = new HashMap<>();
    private Map<String, String> map_nombresUsuarios = new HashMap<>();

    /**
     * Objeto utilizado para gestionar los datos.
     */
    private clsGestorDatos obj_gld;

    /**
     * Constructor de la clase clsGestorLN.
     * Inicializa la lista de usuarios y el mapa de vehiculos.
     * 
     * @author unai.pinilla
     */
    public clsGestorLN() 
    {

        set_usuarios = new TreeSet<>();
        set_usuariosClientes = new TreeSet<>();
        set_usuariosMecanicos = new TreeSet<>();

        map_vehiculos = new HashMap<>();

        obj_gld = new clsGestorDatos();

        obj_gld.vo_conectar();

        vo_inicializar();
    }

    /**
     * Metodo que inicializa la lista de usuarios y el mapa de vehiculos
     * con datos de ejemplo.
     */
    public void vo_inicializar() {

        /* 
        set_usuarios.add(new clsUsuario("admin",
                "admin", "admin",
                "admin@admin.com", 0, 2,
                "admin", "admin"));
        
        clsUsuario c = new clsCliente("Cliente1",
                "Apellido1", "DNI1",
                "Email1", 943000000,
                1, "Direccion1", "Visa1",
                null, null);
        set_usuarios.add(c);
        
        clsUsuario g = new clsGerente("Gerente",
                "Apellido Gerente", "DniG",
                "gerente@gmail.com", 945111111,
            2, null, null, 0, 0, 0);
        //set_usuarios.add(g);

        clsUsuario m = new clsMecanico("Mecanico",
                "Apellido mecanico", "11223344W",
                "mecanico@gmail.com", 666111555,
                4, null, null,
                "Mecanico", "1000");

        //set_usuarios.add(m);
        */
        clsVehiculo v1 = new clsVehiculo("Rojo",
            "8935 FVK", "Audi", 10000,
            "V6");
        map_vehiculos.put("matricula1", v1);

        clsVehiculo v2 = new clsVehiculo("Negro",
            "2531 LNW", "Seat", 40000,
            "TSI");
        map_vehiculos.put("matricula2", v2);

        clsVehiculo v3 = new clsVehiculo("Blanco",
            "5701 DRT", "BMW", 100000,
            "L3");
        map_vehiculos.put("matricula3", v3);

        ResultSet rs = obj_gld.arrl_recuperarInformacionClientes();

        // recorrer este rs y cargo los datos en el conjuntos de clientes

        try 
        {
            while (rs.next()) 
            {
                String nom = rs.getString(1);
                String apellido = rs.getString(2);
                String dni = rs.getString(3);
                String email = rs.getString(4);
                int telefono = rs.getInt(5);
                int rol = rs.getInt(6);
                String dir = rs.getString(7);
                String visa = rs.getString(8);
                String pass = rs.getString(9);
                String user = rs.getString(10);

                set_usuarios.add(new clsCliente(nom, apellido, dni, email,
                        telefono, rol, dir, visa, pass, user));

                arrl_dniClientes.add(dni);

            }
        } catch (SQLException e) 
        {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que permite insertar un nuevo cliente en la lista de usuarios.
     * 
     * @param str_nombre         Nombre del cliente.
     * @param str_apellido       Apellido del cliente.
     * @param str_dni            DNI del cliente.
     * @param str_email          Email del cliente.
     * @param int_telefono       Numero de telefono del cliente.
     * @param int_rol            Rol del cliente.
     * @param str_contraseña     Contraseña del usuario cliente.
     * @param str_nombreUsuario  Nombre del usuario cliente.
     * @param str_direccion      Direccion del cliente.
     * @param str_numVisa        Numero de Visa del cliente.
     * 
     * @return True si el cliente se inserta correctamente, False en caso contrario.
     * 
     * @throws clsDNIFormatoIncorrecto Si el formato del DNI es incorrecto.
     */
    public boolean vo_insertarCliente(String str_nombre, String str_apellido,
            String str_dni, String str_email, int int_telefono, int int_rol,
            String str_contraseña, String str_nombreUsuario, String str_direccion,
            String str_numVisa) throws clsDNIFormatoIncorrecto 
    {
        if (str_nombre.isEmpty() || str_apellido.isEmpty() ||
                str_dni.isEmpty() || str_email.isEmpty() || 
                str_numVisa.isEmpty() || str_direccion.isEmpty()) 
        {
            return false;
        } else if (bln_validarDNI(str_dni) == false) 
        {
            throw new clsDNIFormatoIncorrecto(str_dni);
        } else 
        {
            set_usuarios.add(new clsCliente(str_nombre, str_apellido,
                    str_dni, str_email, int_telefono, int_rol, str_contraseña,
                    str_nombreUsuario, str_direccion, str_numVisa));

            set_usuariosClientes.add(new clsCliente(str_nombre, str_apellido,
                    str_dni, str_email, int_telefono, int_rol, str_contraseña,
                    str_nombreUsuario, str_direccion, str_numVisa));

            obj_gld.vo_nuevoCliente(str_nombre, str_apellido, str_dni, str_email, 
                    int_telefono, int_rol, str_direccion,
                    str_numVisa, str_contraseña, str_nombreUsuario);

            return true;
        }

    }

    /**
     * Metodo que valida el formato del DNI.
     * 
     * @param str_dni DNI que se desea validar.
     * 
     * @return True si el DNI es valido, False en caso contrario.
     */
    private boolean bln_validarDNI(String str_dni)
    {
        String str_letras = "TRWAGMYFPDXBNJZSQVHLCKE";

        if (str_dni == null || str_dni.length() != 9) 
        {
            return false;
        } else 
        {
            String str_numero = str_dni.substring(0, 8);
            char ch_letra = str_dni.charAt(8);
            int int_numDNI;
            try 
            {
                int_numDNI = Integer.parseInt(str_numero);
                int int_indice = int_numDNI % 23;
                char ch_letraCalculada = str_letras.charAt(int_indice);
                return ch_letra == ch_letraCalculada;

            } catch (NumberFormatException e) 
            {
                return false;
            }
        }
    }

    /**
     * Metodo que permite insertar un nuevo gerente en la lista de usuarios.
     * 
     * @param str_nombre        Nombre del gerente.
     * @param str_apellido      Apellido del gerente.
     * @param str_dni           DNI del gerente.
     * @param str_email         Email del gerente.
     * @param int_telefono      Numero de telefono del gerente.
     * @param int_rol           Rol del gerente.
     * @param str_contraseña    Contraseña del usuario gerente (admin)
     * @param str_nombreUsuario Nombre del usuario gerente (admin)
     * @param str_beneficio     Beneficio del gerente.
     * @param str_inversion     Inversión del gerente.
     * @param str_propiedades   Propiedades del gerente.
     */
    public void vo_insertarGerente(String str_nombre, String str_apellido,
        String str_dni, String str_email, int int_telefono, int int_rol,
        String str_contraseña, String str_nombreUsuario, double str_beneficio,
        double str_inversion, double str_propiedades) 
    {
        set_usuarios.add(new clsGerente(str_nombre, str_apellido,
            str_dni, str_email, int_telefono, int_rol, str_contraseña,
            str_nombreUsuario, str_beneficio, str_inversion, str_propiedades));
    }

    /**
     * Metodo que permite insertar un nuevo mecanico en la lista de usuarios.
     * 
     * @param str_nombre        Nombre del mecanico.
     * @param str_apellido      Apellido del mecanico.
     * @param str_dni           DNI del mecanico.
     * @param str_email         Email del mecanico.
     * @param int_telefono      Numero de telefono del mecanico.
     * @param int_rol           Rol del mecanico.
     * @param str_contraseña    Contraseña del usuario mecanico.
     * @param str_nombreUsuario Nombre de usuario del usuario mecanico.
     * @param str_puesto        Puesto del mecanico.
     * @param str_sueldo        Sueldo del mecanico.
     */
    public void vo_insertarMecanico(String str_nombre, String str_apellido,
            String str_dni, String str_email, int int_telefono, int int_rol,
            String str_contraseña, String str_nombreUsuario, String str_puesto,
            String str_sueldo) 
    {
        set_usuarios.add(new clsMecanico(str_nombre, str_apellido,
                str_dni, str_email, int_telefono, int_rol, str_contraseña,
                str_nombreUsuario, str_puesto, str_sueldo));

        set_usuariosMecanicos.add(new clsMecanico(str_nombre, str_apellido,
                str_dni, str_email, int_telefono, int_rol, str_contraseña,
                str_nombreUsuario, str_puesto, str_sueldo));
    }

    /**
     * Metodo que permite borrar un usuario de la lista segun su DNI.
     * 
     * @param str_param_dni DNI del usuario a borrar.
     * @return True si se elimino con exito, false en caso contrario.
     */
    public boolean bln_borrarUsuario(String str_param_dni) 
    {
        Iterator<clsUsuario> it = set_usuarios.iterator();

        while (it.hasNext()) 
        {
            clsUsuario u = it.next();
            if (u.getDni().equals(str_param_dni)) 
            {
                it.remove();
                obj_gld.vo_eliminarUsuario(str_param_dni);
                return true;
            }
        }
        return false;
    }

    /**
     * Metodo que busca un usuario en la lista segun su DNI.
     * 
     * @param str_param_dni DNI del usuario a buscar.
     * @return El usuario encontrado o null si no se encontro.
     */
    public clsUsuario cls_buscarUsuario(String str_param_dni) 
    {
        Iterator<clsUsuario> it = set_usuarios.iterator();

        while (it.hasNext()) 
        {
            clsUsuario u = it.next();
            if (u.getDni().equals(str_param_dni)) 
            {
                return u;
            }
        }

        return null;

    }

    /**
     * Metodo que permite insertar un nuevo coche en el mapa de vehiculos.
     * 
     * @param str_color          Color del coche.
     * @param str_matricula      Matricula del coche.
     * @param str_modelo         Modelo del coche.
     * @param int_kilometraje    Kilometraje del coche.
     * @param str_motor          Motor del coche.
     * @param str_numeroPuertas  Numero de puertas del coche.
     * @param str_numeroAsientos Numero de asientos del coche.
     * @param str_tipoCambio     Tipo de cambio del coche.
     */
    public void vo_insertarCoche(String str_color, String str_matricula,
            String str_modelo, int int_kilometraje, String str_motor,
            int str_numeroPuertas, int str_numeroAsientos,
            String str_tipoCambio) 
    {

        clsCoche c = new clsCoche(str_color, str_matricula,
                str_modelo, int_kilometraje, str_motor, str_numeroPuertas,
                str_numeroAsientos, str_tipoCambio);

        map_vehiculos.put(str_matricula, c);
    }

    /**
     * Metodo que permite borrar un vehiculo del mapa segun su matricula.
     * 
     * @param matricula Matricula del vehículo a borrar.
     * @return True si se elimino con exito, false en caso contrario.
     */
    public boolean bln_borrarVehiculo(String matricula) 
    {
        clsVehiculo v = map_vehiculos.remove(matricula);

        if (v != null)
            return true;
        else
            return false;
    }

    /**
     * Metodo que busca un vehiculo en el mapa segun su matricula.
     * 
     * @param matricula Matricula del vehiculo a buscar.
     * @return El vehiculo encontrado o null si no se encontro.
     */
    public clsVehiculo cls_buscarVehiculo(String matricula) 
    {
        return map_vehiculos.get(matricula);
    }

    /**
     * Metodo que permite recuperar la lista de usuarios.
     * 
     * @return Lista de usuarios.
     */
    public ArrayList<itfProperty> arrl_recuperarUsuarios() 
    {

        ArrayList<itfProperty> r = new ArrayList<>();

        r.addAll(set_usuarios);

        return r;
    }

    /**
     * Metodo que permite recuperar la lista de vehiculos ordenados por
     * matricula.
     * 
     * @return Lista de vehículos ordenados por matrícula
     */
    public ArrayList<itfProperty> arrl_recuperarVehiculosOrdenadosPorMatricula() 
    {
        ArrayList<clsVehiculo> r = new ArrayList<>();

        r.addAll(map_vehiculos.values());

        Collections.sort(r, new clsComparadorVehiculos());

        ArrayList<itfProperty> datos = new ArrayList<>();
        datos.addAll(r);
        return datos;
    }

    /**
     * Metodo que crea un nuevo usuario y lo añade a la lista de usuarios.
     * 
     * @param str_nombre        Nombre del usuario.
     * @param str_apellido      Apellido del usuario.
     * @param str_dni           DNI del usuario.
     * @param str_email         Email del usuario.
     * @param int_telefono      Numero de telefono del usuario.
     * @param int_rol           Rol del usuario.
     * @param str_contraseña    Contraseña del usuario.
     * @param str_nombreUsuario Nombre del usuario.
     * 
     * @return El nuevo usuario creado.
     * 
     * @throws clsUsuarioRepetido Si el usuario ya existe en la lista.
     * 
     * @author unai.pinilla
     */
    public clsUsuario cls_nuevoUsuario(String str_nombre, String str_apellido,
            String str_dni, String str_email, int int_telefono, int int_rol,
            String str_contraseña, String str_nombreUsuario) 
            throws clsUsuarioRepetido
    {

        clsUsuario objP1;

        objP1 = new clsUsuario(str_nombre, str_apellido, str_dni, str_email,
                int_telefono, int_rol, str_contraseña, str_nombreUsuario);

        if (this.cls_buscarUsuario(str_dni) == null)
            set_usuarios.add(objP1);
        else 
        {
            throw new clsUsuarioRepetido("Usuario repetido");
        }
        return objP1;
    }

    /**
     * Metodo que obtiene la lista de usuarios.
     * 
     * @author unai.pinilla
     * @return La lista de usuarios.
     */
    public ArrayList<itfProperty> arrl_listaUsuarios() 
    {

        ArrayList<itfProperty> arrl_resultado;

        arrl_resultado = new ArrayList<>();

        arrl_resultado.addAll(this.set_usuarios);

        return arrl_resultado;

    }

    /**
     * Metodo que elimina un usuario de la lista de usuarios segun su DNI.
     * 
     * @param str_dni DNI del usuario a eliminar.
     * @return True si el usuario fue eliminado correctamente, 
     *         False si no se encontró el usuario.
     * 
     * @author unai.pinilla
     */
    public boolean vo_eliminarUsuario(String str_dni) 
    {
        clsUsuario obj_usuarioEliminado;

        obj_usuarioEliminado = null;
        for (clsUsuario obj_auxUsuario : this.set_usuarios) 
        {
            if (obj_auxUsuario.getDni().equals(str_dni)) 
            {
                obj_usuarioEliminado = obj_auxUsuario;
                this.set_usuarios.remove(obj_usuarioEliminado);
                return true;
            }
        }

        return false;

    }

    /**
     * Metodo que modifica los datos de un usuario segun el DNI que se pida.
     * 
     * @author unai.pinilla
     * @param str_dni      DNI del usuario a modificar.
     * @param str_nombre   Nuevo nombre del usuario.
     * @param str_apellido Nuevo apellido del usuario.
     */
    public void vo_modificarUsuario(String str_nombre, String str_apellido,
            String str_dni) 
    {
        String str_nuevoNombre;
        String str_nuevoApellido;
        String str_nuevoDni;

        str_nuevoNombre = "";
        str_nuevoApellido = "";
        str_nuevoDni = "";

        for (clsUsuario obj_auxUsuario : this.set_usuarios) 
        {
            if (obj_auxUsuario.getDni().equals(str_dni)) 
            {
                obj_auxUsuario.setNombre(str_nuevoNombre);
                obj_auxUsuario.setApellidos(str_nuevoApellido);
                obj_auxUsuario.setDni(str_nuevoDni);


                return;
            }
        }
    }

    /**
     * Método público que permite modificar un vehículo existente.
     * 
     * @param str_matricula        Matrícula del vehículo a modificar.
     * @param str_nuevoColor       Nuevo color del vehículo.
     * @param str_nuevoModelo      Nuevo modelo del vehículo.
     * @param int_nuevosKilometros Nuevos kilómetros del vehículo.
     * @param str_nuevoMotor       Nuevo motor del vehículo.
     */
    public void vo_modificarVehiculo(String str_matricula,
            String str_nuevoColor, String str_nuevoModelo, 
            int int_nuevosKilometros, String str_nuevoMotor) 
    {
        clsVehiculo vehiculo = map_vehiculos.get(str_matricula);

        if (vehiculo != null) 
        {
            vehiculo.setcolor(str_nuevoColor);
            vehiculo.setmodelo(str_nuevoModelo);
            vehiculo.setkilometraje(int_nuevosKilometros);
            vehiculo.setmotor(str_nuevoMotor);
            return;
        }
    }

    /**
     * Metodo que obtiene las credenciales de un usuario basado en su nombre.
     * 
     * @param str_usuario Nombre del usuario del cual se desean obtener las
     *                    credenciales.
     * @return El rol del usuario si se encuentra, o 2 si no se encuentra.
     */
    public int int_obtenerCredenciales(String str_usuario) 
    {

        for (clsUsuario u : set_usuarios) 
        {
            if (u.getNombre().equals(str_usuario)) 
            {
                return u.getRol();
            }
        }

        return 2;
    }

    /**
     * Metodo que recupera la informacion de todos los usuarios en un ArrayList.
     * 
     * @return Un ArrayList con la informacion de todos los usuarios.
     */
    public ArrayList<String> arrl_recuperarInformacionUsuarios() 
    {

        ArrayList<String> infoUsuarios = new ArrayList<>();

        String info = "";

        // recorro cada jugador de la lista
        for (clsUsuario objUsuario : set_usuarios) {
            // añado a ese texto la informacion del jugador que quiero mostrar
            info = objUsuario.getNombre() + "-" + objUsuario.getApellidos() +
                    "-" + objUsuario.getRol();

            // esa info del jugador se añade a la respuesta que voy a enviar
            // al clsMenu
            infoUsuarios.add(info);
        }
        // le paso al clsMenu la informacion de todos
        return infoUsuarios;

    }

    /**
     * Metodo que verifica si un usuario esta registrado basado en su nombre.
     * 
     * @param str_nombre Nombre del usuario a verificar.
     * @return True si el usuario esta registrado, False en caso contrario.
     */
    public boolean bln_esUsuarioRegistrado(String str_nombre) 
    {

        for (clsUsuario u : set_usuarios) 
        {
            if (u.getNombre().equals(str_nombre)) 
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Metodo que obtiene una lista de clientes filtrados por direccion.
     * 
     * @return Un ArrayList con los clientes ordenados por direccion.
     */
    public ArrayList<itfProperty> arrl_obtenerClientesPorDireccion() 
    {
        List<clsCliente> listaClientes = new ArrayList<>(set_usuariosClientes);
        Collections.sort(listaClientes, new clsComparadorClientes());

        ArrayList<itfProperty> clientesOrdenados = new ArrayList<>();
        clientesOrdenados.addAll(listaClientes);

        return clientesOrdenados;
    }

    /**
     * Metodo que obtiene una lista de todos los vehiculos.
     * 
     * @return Un ArrayList con todos los vehiculos.
     */
    public ArrayList<itfProperty> arrl_obtenerVehiculos() 
    {

        ArrayList<itfProperty> resultado = new ArrayList<>();

        resultado.addAll(map_vehiculos.values());

        return resultado;

    }

    /**
     * Metodo que obtiene una lista de vehiculos ordenados por matricula.
     * 
     * @return Un ArrayList con los vehiculos ordenados por matricula.
     */
    public ArrayList<itfProperty> arrl_obtenerVehiculosPorMatricula() 
    {

        List<clsVehiculo> listaCoches = new ArrayList<>(map_vehiculos.values());
        Collections.sort(listaCoches, new clsComparadorVehiculos());

        ArrayList<itfProperty> cochesOrdenados = new ArrayList<>();
        cochesOrdenados.addAll(listaCoches);

        return cochesOrdenados;
    }

    /**
     * Metodo que sirve para almacenar los usuarios registrados.
     * 
     * @param str_nombreUsuario Nombre del usuario registrado.
     * @param str_dni           DNI del usuario registrado.
     * @param str_contraseña    Contraseña del usuario registrado.
     * 
     * @return True si el usuario ha sido registrado correctamente,
     *         False en caso contrario.
     */
    public boolean bln_almacenarDNIRegistro(String str_nombreUsuario,
            String str_dni, String str_contraseña) 
    {
        if (str_nombreUsuario.isEmpty() && str_dni.isEmpty() &&
                str_contraseña.isEmpty()) 
        {
            return false;
        } else 
        {
            arrl_dniRegistrados.add(str_dni);
            map_contraseñas.put(str_dni, str_contraseña);
            map_nombresUsuarios.put(str_dni, str_nombreUsuario);
            return true;
        }
    }

    /**
     * Metodo que sirve para almacenar los DNI de los usuarios de tipo
     * cliente añadidos por el gerente o por un mecanico.
     * 
     * @param str_dni DNI del cliente registrado.
     * 
     * @return False si el DNI es un string vacio y true una vez se añade a
     *         la lista de DNI
     */
    public boolean bln_almacenarDNICliente(String str_dni) 
    {
        if (str_dni.isEmpty())
        {
            return false;
        } else 
        {
            arrl_dniClientes.add(str_dni);
            return true;
        }
    }

    /**
     * Metodo que sirve para almacenar los DNI de los usuarios de tipo
     * mecanico añadidos por el gerente.
     * 
     * @param str_dni DNI del mecanico registrado.
     * 
     * @return False si el DNI es un string vacio y true una vez se añade a
     *         la lista de DNI
     */
    public boolean bln_almacenarDNIMecanico(String str_dni) 
    {
        if (str_dni.isEmpty()) 
        {
            return false;
        } else 
        {
            arrl_dniMecanicos.add(str_dni);
            return true;
        }
    }

    /**
     * Metodo que verifica que el usuario, contraseña y dni que introduce el
     * cliente son validos.
     * 
     * @param str_nombredeUsuario Nombre del usuario registrado.
     * @param str_dni           DNI del usuario registrado.
     * @param str_contraseña    Contraseña del usuario registrado.
     * 
     * @return True si el usuario es valido y False en caso contrario.
     */
    public boolean bln_verificarCredencialesCliente(String str_nombredeUsuario,
            String str_dni, String str_contraseña) 
    {
        if (arrl_dniClientes.contains(str_dni) &&
                map_contraseñas.getOrDefault(str_dni, "").
                equals(str_contraseña)) 
        {
            return true;
        } else 
        {
            return false;
        }
    }

    /**
     * Metodo que verifica que el usuario, contraseña y dni que introduce el
     * mecanico son validos.
     * 
     * @param str_nombredeUsuario Nombre del usuario registrado.
     * @param str_dni           DNI del usuario registrado.
     * @param str_contraseña    Contraseña del usuario registrado.
     * 
     * @return True si el usuario es valido y False en caso contrario.
     */
    public boolean bln_verificarCredencialesMecanico(String str_nombredeUsuario,
            String str_dni, String str_contraseña) 
    {
        if (arrl_dniMecanicos.contains(str_dni) &&
                map_contraseñas.getOrDefault(str_dni, "").
                equals(str_contraseña)) 
        {
            return true;
        } else 
        {
            return false;
        }
    }

    /**
     * Metodo que obtiene el rol del usuario segun su nombre de usuario y 
     * contraseña.
     * 
     * @param str_user Nombre del usuario.
     * @param str_pass Contraseña del usuario.
     * 
     * @return El rol del usuario si las credenciales son validas, -1 en caso 
     *         contrario.
     */
    public int int_obtenerCredencialesUserPass(String str_user, String str_pass) 
    {

        for (clsUsuario u : set_usuarios) 
        {
            if (u.getNombreUsuario().equals(str_user) && u.getContraseña().
            equals(str_pass)) {
                return u.getRol();
            }
        }

        // -1 es un rol que no existe
        return -1;

    }

    /**
     * Metodo que recupera los DNIs de todos los clientes.
     * 
     * @return Una lista de DNIs de los clientes.
     */
    public ArrayList<String> arrl_recuperarDNIsClientes() 
    {

        return this.arrl_dniClientes;
    }

    /**
     * Metodo que recupera el nombre del cliente segun su DNI.
     * 
     * @param str_dni DNI del cliente.
     * 
     * @return El nombre del cliente si se encuentra, null en caso contrario.
     */
    public String str_recuperarNombreCliente(String str_dni) 
    {

        for (clsUsuario u : this.set_usuarios) 
        {
            if (u.getDni().equals(str_dni))
                return u.getNombre();
        }
        return null;
    }

    /**
     * Metodo que recupera el apellido del cliente segun su DNI.
     * 
     * @param str_dni DNI del cliente.
     * 
     * @return El apellido del cliente si se encuentra, null en caso contrario.
     */
    public String str_recuperarApellidoCliente(String str_dni) 
    {
        for (clsUsuario u : this.set_usuarios) 
        {
            if (u.getDni().equals(str_dni))
                return u.getApellidos();
        }
        return null;
    }

    /**
     * Metodo que recupera el telefono del cliente segun su DNI.
     * 
     * @param str_dni DNI del cliente.
     * 
     * @return El telefono del cliente si se encuentra, null en caso contrario.
     */
    public String str_recuperarTlfCliente(String str_dni) 
    {
        for (clsUsuario u : this.set_usuarios) 
        {
            if (u.getDni().equals(str_dni))
                return "" + u.getTelefono();
        }
        return null;
    }

    /**
     * Metodo que recupera el nombre de usuario del cliente segun su DNI.
     * 
     * @param str_dni DNI del cliente.
     * 
     * @return El nombre de usuario del cliente si se encuentra, null en caso 
     *          contrario.
     */
    public String str_recuperarUserCliente(String str_dni) 
    {
        for (clsUsuario u : this.set_usuarios) 
        {
            if (u.getDni().equals(str_dni))
                return u.getNombreUsuario();
        }
        return null;
    }

    /**
     * Metodo que recupera la contraseña del cliente segun su DNI.
     * 
     * @param str_dni DNI del cliente.
     * 
     * @return La contraseña del cliente si se encuentra, null en caso contrario
     */
    public String str_recuperarPassCliente(String str_dni) 
    {
        for (clsUsuario u : this.set_usuarios) 
        {
            if (u.getDni().equals(str_dni))
                return u.getContraseña();
        }
        return null;
    }

    /**
     * Metodo que modifica la informacion del cliente segun su DNI.
     * 
     * @param str_dni      DNI del cliente.
     * @param str_nombre   Nuevo nombre del cliente.
     * @param str_apellido Nuevo apellido del cliente.
     * @param int_rol      Nuevo rol del cliente.
     * @param str_user     Nuevo nombre de usuario del cliente.
     * @param str_pass     Nueva contraseña del cliente.
     */
    public void vo_modificarCliente(String str_dni, String str_nombre, 
                                String str_apellido, int int_rol, 
                                String str_user, String str_pass) 
    {

        for (clsUsuario u : set_usuarios) {
            if (u.getDni().equals(str_dni)) {
                u.setNombre(str_nombre);
                u.setApellidos(str_apellido);
                u.setRol( int_rol );
                u.setNombreUsuario(str_user);
                u.setcontraseña(str_pass);

                obj_gld.vo_modificarCliente( str_dni , str_nombre , str_apellido , 
                                        int_rol , str_user , str_pass );

                return;
            }
        }
    }

    /**
     * Metodo que devuelve una lista de clientes ordenados por su DNI.
     * 
     * @return Una lista de clientes ordenados por su DNI.
     */
    public ArrayList<itfProperty> arrl_getClientesOrdenadosDNI() 
    {
        ArrayList<itfProperty> aux = new ArrayList<>();
        aux.addAll(set_usuarios);
        System.out.println("DEBUG: Tamaño lista clientes: " + aux.size());
        return aux;
    }

    /**
     * Metodo que devuelve una lista de clientes ordenados por su apellido.
     * 
     * @return Una lista de clientes ordenados por su apellido.
     */
    public List<itfProperty> list_getClientesOrdenadosApellido() 
    {
       
        ArrayList<clsUsuario> aux = new ArrayList<>();
        aux.addAll(set_usuarios);
        Collections.sort( aux , new clsComparadorApellido() );    

        ArrayList<itfProperty> aux2 = new ArrayList<>();
        aux2.addAll(aux);
        return aux2;
    }

    /**
     * Metodo que recupera el rol del cliente segun su DNI.
     * 
     * @param str_dni DNI del cliente.
     * 
     * @return El rol del cliente si se encuentra, -1 en caso contrario.
     */
    public int int_recuperarRolCliente(String str_dni) 
    {
       
        for (clsUsuario u : this.set_usuarios) 
        {
            if (u.getDni().equals(str_dni))
                return u.getRol();
        }
        return -1;


    }

}
