package LN;

import java.util.Comparator;

/**
 * Clase que implementa un comparador para ClsCliente. Comparará objetos de 
 * dicha clase dependiendo de sus direcciones
 * Ademas permite ordenar los clientes en orden ascendente o descendente.
 * 
 * @author u.garay
 * @version 4.0
 */
public class clsComparadorClientes implements Comparator<clsCliente> {

    /**
     * Compara dos objetos clsCliente segun sus direcciones.
     * 
     * @author u.garay
     * 
     * @param o1 El primer objeto a comparar clsCliente.
     * @param o2 El segundo objeto clsCliente a comparar.
     * @return Un valor negativo si o1 es menor que o2, un valor positivo si es
     * lo contrario o cero si ambos objetos tienen la misma direccion.
     */
    @Override
    public int compare(clsCliente o1, clsCliente o2) {
       
        return o1.getDireccion().compareTo(o2.getDireccion());
    }
    
}
