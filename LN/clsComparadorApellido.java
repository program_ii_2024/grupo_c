package LN;

import java.util.Comparator;

/**
 * Clase que implementa la interfaz Comparator para comparar objetos clsUsuario 
 * por apellido.
 */
public class clsComparadorApellido implements Comparator<clsUsuario> {

    /**
     * Compara dos objetos clsUsuario por sus apellidos.
     * 
     * @param o1 El primer objeto clsUsuario a comparar.
     * @param o2 El segundo objeto clsUsuario a comparar.
     * @return Un valor negativo si o1 es menor que o2, cero si son iguales, o 
     *         un valor positivo si o1 es mayor que o2.
     */
    @Override
    public int compare(clsUsuario o1, clsUsuario o2) {
        return o1.getApellidos().compareTo(o2.getApellidos());
    }

}
