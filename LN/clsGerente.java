package LN;

import COMUN.clsConstantes;

/**
 * La clase clsGerente representa a un gerente que hereda de la
 * clase clsUsuario.
 * 
 * @author izaro.ortega
 * @version 4.0
 */
public class clsGerente extends clsUsuario 
{

    /**
     * Obtiene el valor de un propiedad del gerente.
     * 
     * @param propiedad La propiedad cuyo valor se quiere obtener.
     * @return El valor de la propiedad especificada.
     * 
     * @author izaro.ortega
     */
    @Override
    public Object getObjectProperty(String propiedad) 
    {
        switch (propiedad) {

            case clsConstantes.BENEFICIO:
                return dbl_beneficio;

            case clsConstantes.INVERSION:
                return dbl_inversion;

            case clsConstantes.PROPIEDADES:
                return dbl_propiedades;

            default:
                return super.getObjectProperty(propiedad);
        }
    }

    /**
     * El beneficio del gerente
     * 
     * @author izaro.ortega
     * 
     */
    private double dbl_beneficio;
    /**
     * La inversion del gerente
     * 
     * @author izaro.ortega
     */
    private double dbl_inversion;
    /**
     * Las propiedades del gerente
     * 
     * @author izaro.ortega
     */
    private double dbl_propiedades;

    /**
     * Constructor por defecto para crear una instancia de clsGerente con 
     * valores iniciales.
     * 
     * @author izaro.ortega
     */

    public clsGerente() 
    {
        this.dbl_beneficio = 0;
        this.dbl_inversion = 0;
        this.dbl_propiedades = 0;
    }

    /**
     * Constructor para crear una instancia de clsGerente con valores
     * iniciales de beneficios, inversion y propiedades.
     * 
     * @param str_nombre       Nombre del gerente.
     * @param str_apellido     Apellido del gerente.
     * @param str_dni          DNI del gerente.
     * @param str_email        Correo electronico del gerente.
     * @param int_telefono     Numero de telefono del gerente.
     * @param int_rol          Rol del gerente.
     * @param str_contraseña   Contraseña del gerente.
     * @param str_nombreUsuario Nombre de usuario del gerente.
     * @param dbl_beneficio    El beneficio inicial del gerente.
     * @param dbl_inversion    La inversión inicial del gerente.
     * @param dbl_propiedades  Las propiedades iniciales del gerente.
     * 
     * @author izaro.ortega
     */
    public clsGerente(String str_nombre, String str_apellido,
            String str_dni, String str_email, int int_telefono, int int_rol,
            String str_contraseña, String str_nombreUsuario,
            double dbl_beneficio, double dbl_inversion, double dbl_propiedades) 
    {

        super(str_nombre, str_apellido, str_dni, str_email, int_telefono,
        int_rol, str_contraseña, str_nombreUsuario);
        this.dbl_beneficio = dbl_beneficio;
        this.dbl_inversion = dbl_inversion;
        this.dbl_propiedades = dbl_propiedades;
    }

    /**
     * Obtiene los beneficios del gerente.
     *
     * @return Los beneficios del gerente.
     * 
     * @author izaro.ortega
     */

    public double getBeneficios() 
    {
        return this.dbl_beneficio;
    }

    /**
     * Establece los bebficios del gerente
     * 
     * @param dbl_beneficio El nuevo beneficio del gerente
     * 
     * @author izaro.ortega
     */

    public void setBeneficios(double dbl_beneficio) 
    {
        this.dbl_beneficio = dbl_beneficio;
    }

    /**
     * Obtiene la inversion del gerente.
     * 
     * @return La inversion del gerente.
     * 
     * @author izaro.ortega
     */

    public double getInversion() 
    {
        return this.dbl_inversion;
    }

    /**
     * Establece la inversion del gerente.
     * 
     * @param dbl_inversion La nueva inversion del gerente.
     * 
     * @author izaro.ortega
     */

    public void setInversion(double dbl_inversion)
    {
        this.dbl_inversion = dbl_inversion;
    }

    /**
     * Obtiene las propiedades del gerente.
     * 
     * @return Las propiedades del gerente.
     * 
     * @author izaro.ortega
     */

    public double getPropiedades() 
    {
        return this.dbl_propiedades;
    }

    /**
     * Establece las propiedades del gerente.
     * 
     * @param dbl_propiedades Las nuevas propiedades del gerente.
     * 
     * @author izaro.ortega
     */

    public void setPropiedades(double dbl_propiedades) 
    {
        this.dbl_propiedades = dbl_propiedades;
    }

}
