package LN;

import COMUN.itfProperty;

import COMUN.clsConstantes;
/**
 * Clase que representa un vehiculo.
 * Es hija de la clase clsCliente e incluye informacion adicional como el
 * color, la matricula, el modelo, el kilometraje y el motor del vehiculo.
 * Ademas permite comparar los vehiculos entre ellos.
 * Implementa la interfaz itfProperty y es comparable.
 * 
 * @author u.garay
 * @version 3.0
 */
public class clsVehiculo implements itfProperty ,  Comparable<clsVehiculo>  {

    /**
     * Metodo para calcular el hash del vehiculo.
     * 
     * @author u.garay
     * 
     * @return El hash del vehiculo.
     */
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((str_matricula == null) ? 0 : 
        str_matricula.hashCode());
        return result;
    }

    /**
     * Metodo para comparar si dos vehiculos son iguales.
     * 
     * @author u.garay
     * 
     * @param obj El objeto/vehiculo con el que se compara.
     * @return True si los vehiculos son iguales, false en caso contrario.
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        clsVehiculo other = (clsVehiculo) obj;
        if (str_matricula == null) {
            if (other.str_matricula != null)
                return false;
        } else if (!str_matricula.equals(other.str_matricula))
            return false;
        return true;
    }

        /**
     * Metodo que compara dos vehiculos por su kilometraje.
     * 
     * @author u.garay 
     * 
     * @param v El vehiculo con el que se compara.
     * @return Un numero negativo si el de este vehiculo es menor que el del
     * vehiculo v, cero si son iguales, o un numero positivo si el de este es  
     * mayor.
     */
    public int compareTo( clsVehiculo v )
    {
        return this.int_kilometraje - v.int_kilometraje;
    }

    /**
     * Metodo para obtener una propiedad del objeto vehiculo.
     * 
     * @author u.garay
     * 
     * @param propiedad La propiedad que se desea obtener.
     * @return El valor de la propiedad.
     */
    @Override
    public Object getObjectProperty(String propiedad) 
    {
        
        switch (propiedad) {
            
            case clsConstantes.COLOR : return str_color;
            case clsConstantes.MATRICULA : return str_matricula;
            case clsConstantes.MODELO : return str_modelo;
            case clsConstantes.KILOMETRAJE : return int_kilometraje;
            case clsConstantes.MOTOR : return str_motor;
         
            default:
                return null;
        }        
    }

   
    /**
     * Atributos de la clase clsVehiculo.
     * 
     * @author u.garay
     */

    private String str_color;
    private String str_matricula;
    private String str_modelo;
    private int int_kilometraje;
    private String str_motor;

    /**
     * Constructor de la clase clsVehiculo.
     * 
     * @author u.garay
     * 
     * Inicializa vehiculo con los valores predeterminados.
     */
    public clsVehiculo() 
    {
        this.str_color = "";
        this.str_matricula = "";
        this.str_modelo = "";
        this.int_kilometraje = 0;
        this.str_motor = "";
    }

    /**
     * Constructor de la clase clsVehiculo.
     * 
     * @author u.garay
     * 
     * @param str_color       El color del vehiculo.
     * @param str_matricula   La matricula del vehiculo.
     * @param str_modelo      El modelo del vehiculo.
     * @param int_kilometraje El kilometraje del vehiculo.
     * @param str_motor       El motor del vehiculo.
     */
    public clsVehiculo(String str_color, String str_matricula,
        String str_modelo, int int_kilometraje, String str_motor) 
    {
        this.str_color = str_color;
        this.str_matricula = str_matricula;
        this.str_modelo = str_modelo;
        this.int_kilometraje = int_kilometraje;
        this.str_motor = str_motor;
    }

    /**
     * Metodo para obtener el color del vehiculo.
     * 
     * @author u.garay
     * 
     * @return El color del vehiculo.
     */
    public String getcolor() 
    {
        return this.str_color;
    }

    /**
     * Metodo para establecer el color del vehiculo.
     * 
     * @author u.garay
     * 
     * @param str_color El nuevo color del vehiculo.
     */
    public void setcolor(String str_color) 
    {
        this.str_color = str_color;
    }

    /**
     * Metodo para obtener la matricula del vehiculo.
     * 
     * @author u.garay
     * 
     * @return La matricula del vehiculo.
     */
    public String getmatricula() 
    {
        return this.str_matricula;
    }

    /**
     * Metodo para establecer la matricula del vehiculo.
     * 
     * @author u.garay
     * 
     * @param str_matricula La nueva matricula del vehiculo.
     */
    public void setmatricula(String str_matricula) 
    {
        this.str_matricula = str_matricula;
    }

    /**
     * Metodo para obtener el modelo del vehiculo.
     * 
     * @author u.garay
     * 
     * @return El modelo del vehiculo.
     */
    public String getmodelo() 
    {
        return this.str_modelo;
    }

    /**
     * Metodo para establecer el modelo del vehiculo.
     * 
     * @author u.garay
     * 
     * @param str_modelo El nuevo modelo del vehiculo.
     */
    public void setmodelo(String str_modelo) 
    {
        this.str_modelo = str_modelo;
    }

    /**
     * Metodo para obtener el kilometraje del vehiculo.
     * 
     * @author u.garay
     * 
     * @return El kilometraje del vehiculo.
     */
    public int getkilometraje() 
    {
        return this.int_kilometraje;
    }

    /**
     * Metodo para establecer el kilometraje del vehiculo.
     * 
     * @author u.garay
     * 
     * @param int_kilometraje El nuevo kilometraje del vehiculo
     */
    public void setkilometraje(int int_kilometraje) 
    {
        this.int_kilometraje = int_kilometraje;
    }

    /**
     * Metodo para obtener el motor del vehiculo.
     * 
     * @author u.garay
     * 
     * @return El motor del vehiculo.
     */

    public String getmotor() 
    {
        return this.str_motor;
    }

    /**
     * Metodo para establecer el motor del vehiculo.
     * 
     * @author u.garay
     * 
     * @param str_motor El nuevo motor del vehiculo.
     */
    public void setmotor(String str_motor) 
    {
        this.str_motor = str_motor;
    }

}
