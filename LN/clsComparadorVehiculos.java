package LN;

import java.util.Comparator;

/**
 * Clase que implementa un comparador para clsVehiculo.
 * Compara vehiculos basandose en su mtricula de forma descendente.
 * 
 * @author izaro.ortega
 * @version 4.0
 */

public class clsComparadorVehiculos implements Comparator<clsVehiculo> 
{

    /**
     * Compara dos objetos clsVehiculo segun sus matriculas.
     * 
     * @param v1 El primer vehículo a comparar.
     * @param v2 El segundo vehículo a comparar.
     * @return Un entero negativo, cero o un entero positivo si el primer 
     * vehículo es menor, igual o mayor que el segundo vehículo.
     * 
     * @author izaro.ortega
     */
    @Override
    public int compare(clsVehiculo v1, clsVehiculo v2) 
    {
        return v2.getmatricula().compareTo(v1.getmatricula());
    }

}

