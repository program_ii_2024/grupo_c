package LN;

import COMUN.clsConstantes;

/**
 * Clase que representa un coche.
 * Es hija de la clase clsVehículo.
 * 
 * @author izaro.ortega
 * @version 3.0
 * 
 */
public class clsCoche extends clsVehiculo 
{
    /**
     * Obtiene el valor de una propiedad del coche.
     * 
     * @param propiedad La propiedad cuyo valor se quiere obtener.
     * @return El valor de la propiedad especificada.
     * 
     * @author izaro.ortega
     */
    @Override
    public Object getObjectProperty( String propiedad )
    {
        switch (propiedad) 
        {

            case clsConstantes.NUMERO_PUERTAS: 
             return int_numeroPuertas;      

            case clsConstantes.NUMERO_ASIENTOS: 
             return int_numeroAsientos;    

            case clsConstantes.TIPO_CAMBIO:
             return str_tipoCambio;   

            default:
                return super.getObjectProperty(propiedad);
        }
    }

    /**
     * El numero de puertas del coche.
     * 
     * @author izaro.ortega
     */
    private int int_numeroPuertas;

    /**
     * El numero de asientos del coche.
     * 
     * @author izaro.ortega
     */
    private int int_numeroAsientos;

    /**
     * El tipo de cambio para el coche.
     * 
     * @author izaro.ortega
     */
    private String str_tipoCambio;

    /**
     * Constructor para crear una instancia de clsCoche
     * con valores iniciales.
     * 
     * @author izaro.ortega
     */
    public clsCoche() 
    {
        this.int_numeroPuertas = 0;
        this.int_numeroAsientos = 0;
        this.str_tipoCambio = " ";
    }

    /**
     * Constructor para crear una instancia de clsCoche con
     * valores iniciales de numero de puertas, numero de
     * asientos y tipo de cambio.
     * 
     * @param str_color         Color del coche.
     * @param str_matricula     Matrícula del coche.
     * @param str_modelo        Modelo del coche.
     * @param int_kilometraje   Kilometraje del coche.
     * @param str_motor         Motor del coche.
     * @param int_numeroPuertas Número de puertas del coche.
     * @param int_numeroAsientos Número de asientos del coche.
     * @param str_tipoCambio    Tipo de cambio del coche.
     * 
     * @author izaro.ortega
     */
    public clsCoche(String str_color, String str_matricula,
    String str_modelo, int int_kilometraje, String str_motor , int int_numeroPuertas, int int_numeroAsientos,
    String str_tipoCambio) 
    {
        super(str_color, str_matricula, str_modelo, int_kilometraje, str_motor);
        this.int_numeroPuertas = int_numeroPuertas;
        this.int_numeroAsientos = int_numeroAsientos;
        this.str_tipoCambio = str_tipoCambio;
    }

    /**
     * Obtiene el numero de puertas del coche.
     * 
     * @return El numero de puertas del coche.
     * 
     * @author izaro.ortega
     */
    public int getNumeropuertas() 
    {
        return this.int_numeroPuertas;
    }

    /**
     * Establece el numero de puertas del coche.
     * 
     * @param int_numeroPuertas Numero de puertas del coche
     * 
     * @author izaro.ortega
     */
    public void setNumeropuertas(int int_numeroPuertas) 
    {
        this.int_numeroPuertas = int_numeroPuertas;
    }

    /**
     * Obtiene el numero de asientos del coche.
     * 
     * @return El numero de asientos del coche.
     * 
     * @author izaro.ortega
     */
    public int getNumeroasientos() 
    {
        return this.int_numeroAsientos;
    }

    /**
     * Establece el numero de asientos del coche.
     * 
     * @param str_numeroAsientos Numero de asientos del coche
     * 
     * @author izaro.ortega
     */
    public void setNumeroasientos(int str_numeroAsientos)
    {
        this.int_numeroAsientos = str_numeroAsientos;
    }

    /**
     * Obtiene el tipo de cambio del coche.
     * 
     * @return El tipo de cambio del coche.
     * 
     * @author izaro.ortega
     */
    public String getTipocambio() 
    {
        return this.str_tipoCambio;
    }

    /**
     * Establece el tipo de cambio del coche.
     * 
     * @param str_tipoCambio Tipo de cambio del coche
     * 
     * @author izaro.ortega
     */
    public void setTipocambio(String str_tipoCambio) 
    {
        this.str_tipoCambio = str_tipoCambio;
    }

}
