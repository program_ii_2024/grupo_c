package LD;

import java.sql.*;

import COMUN.clsConstantesLD;

/**
 * Clase que gestiona la conexion y operaciones con la base de datos del
 * sistema.
 * 
 * Esta clase proporciona metodos para conectarse a la base de datos, ejecutar
 * consultas, insertar, modificar y eliminar datos en las tablas de la base de
 * datos.
 * 
 * @author Samul.Aranda
 * @version 4.0
 */
public class clsGestorDatos {
	private static final String URL = "jdbc:mysql://127.0.1:3306/";
	private static final String SCHEMA = "taller_mecanico";
	private static final String PARAMS = "?useUnicode=true&useJDBCCompliantTime"
	+ "zoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=" +
	"false";
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String USER = "root";
	private static final String PASS = "root";

	/**
	 * Objeto para crear la conexion a base de datos.
	 */
	Connection obj_conexion;

	/**
	 * Objeto para crear la consulta a base de datos.
	 */
	PreparedStatement obj_ps;

	/**
	 * Objeto para devolver el resultado de la consulta.
	 */
	ResultSet obj_rs;

	/**
	 * Constructor sin parametros de la clase.
	 */
	public clsGestorDatos() 
	{
		obj_conexion = null;
		obj_ps = null;
		obj_rs = null;
	}

	/**
	 * Metodo para la conexion a la base de datos.
	 */
	public void vo_conectar() 
	{
		try 
		{

			Class.forName(DRIVER);
			obj_conexion = DriverManager.getConnection(URL + SCHEMA + PARAMS, 
														USER, PASS);
			System.out.println("DEBUG: CONECTADO A LA BD");

		} catch (Exception e) 
		{
			System.out.println("NO CONNECTION ");
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Metodo para desconectar la base de datos.
	 */
	public void vo_desconectar() 
	{
		try 
		{
			obj_conexion.close();
			obj_ps.close();
		} catch (SQLException e) 
		{
		} finally 
		{
			try 
			{
				obj_conexion.close();
			} catch (Exception e) 
			{
			}
			try 
			{
				obj_ps.close();
			} catch (Exception e) 
			{
			}
		}
	}

	/**
	 * Metodo para insertar un nuevo cliente en la base de datos.
	 * 
	 * @param str_nombre        Nombre del cliente.
	 * @param str_apellido      Apellido del cliente.
	 * @param str_dni           DNI del cliente.
	 * @param str_email         Email del cliente.
	 * @param int_telefono      Numero de telefono del cliente.
	 * @param int_rol           Rol del cliente.
	 * @param str_direccion     Direccion del cliente.
	 * @param str_numVisa       Numero de tarjeta del cliente.
	 * @param str_contraseña    Contraseña del cliente.
	 * @param str_nombreUsuario Nombre de usuario del cliente.
	 */
	public void vo_nuevoCliente(String str_nombre, String str_apellido,
			String str_dni, String str_email, int int_telefono,
			int int_rol, String str_direccion, String str_numVisa,
			String str_contraseña, String str_nombreUsuario) 
	{
		vo_conectar();
		try 
		{
			obj_ps = obj_conexion.prepareStatement(clsConstantesLD.
													SQL_INSERT_CLIENTE);

			obj_ps.setString(1, str_nombre);
			obj_ps.setString(2, str_apellido);
			obj_ps.setString(3, str_dni);
			obj_ps.setString(4, str_email);
			obj_ps.setInt(5, int_telefono);
			obj_ps.setInt(6, int_rol);
			obj_ps.setString(7, str_direccion);
			obj_ps.setString(8, str_numVisa);
			obj_ps.setString(9, str_contraseña);
			obj_ps.setString(10, str_nombreUsuario);

			obj_ps.executeUpdate();
		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
			vo_desconectar();
		}
	}

	/**
	 * Metodo para recuperar la informacion de los clientes desde la base de datos.
	 * 
	 * @return ResultSet con la informacion de los clientes.
	 */
	public ResultSet arrl_recuperarInformacionClientes() 
	{
		vo_conectar();
		try 
		{
			obj_ps = obj_conexion.prepareStatement(clsConstantesLD.
													SQL_SELECT_CLIENTE);

			obj_rs = obj_ps.executeQuery();
		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}

		return obj_rs;
	}

	/**
	 * Metodo para eliminar un cliente de la base de datos.
	 * 
	 * @param str_dni DNI del cliente a eliminar.
	 */
	public void vo_eliminarCliente(String str_dni) 
	{

		vo_conectar();

		try 
		{

			obj_ps = obj_conexion.prepareStatement(clsConstantesLD.
													SQL_DELETE_CLIENTE);

			obj_ps.setString(3, str_dni);

			obj_ps.executeUpdate();

		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
			vo_desconectar();
		}
	}

	/**
	 * Metodo para modificar los datos de un cliente en la base de datos.
	 * 
	 * @param str_nombre        Nuevo nombre del cliente.
	 * @param str_apellido      Nuevo apellido del cliente.
	 * @param str_dni           DNI del cliente a modificar.
	 * @param str_email         Nuevo email del cliente.
	 * @param int_telefono      Nuevo numero de telefono del cliente.
	 * @param int_rol           Nuevo rol del cliente.
	 * @param str_direccion     Nueva direccion del cliente.
	 * @param str_numVisa       Nuevo numero de tarjeta del cliente.
	 * @param str_contraseña    Nueva contraseña del cliente.
	 * @param str_nombreUsuario Nuevo nombre de usuario del cliente.
	 */
	public void vo_modificarClientes(String str_nombre, String str_apellido,
			String str_dni, String str_email, int int_telefono,
			int int_rol, String str_direccion, String str_numVisa,
			String str_contraseña, String str_nombreUsuario) 
	{

		vo_conectar();
		try 
		{

			obj_ps = obj_conexion.prepareStatement(clsConstantesLD.
													SQL_UPDATE_CLIENTE);

			obj_ps.setString(1, str_nombre);
			obj_ps.setString(2, str_apellido);
			obj_ps.setString(3, str_dni);
			obj_ps.setString(4, str_email);
			obj_ps.setInt(5, int_telefono);
			obj_ps.setInt(6, int_rol);
			obj_ps.setString(7, str_direccion);
			obj_ps.setString(8, str_numVisa);
			obj_ps.setString(9, str_contraseña);
			obj_ps.setString(10, str_nombreUsuario);

			obj_ps.executeUpdate();

		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
			vo_desconectar();
		}
	}

	/**
	 * Metodo para insertar un nuevo gerente en la base de datos.
	 * 
	 * @param str_nombre        Nombre del gerente.
	 * @param str_apellido      Apellido del gerente.
	 * @param str_dni           DNI del gerente.
	 * @param str_email         Email del gerente.
	 * @param int_telefono      Telefono del gerente.
	 * @param int_rol           Rol del gerente.
	 * @param str_contraseña    Contraseña del gerente.
	 * @param str_nombreUsuario Nombre de usuario del gerente.
	 * @param str_beneficio     Beneficio del gerente.
	 * @param str_inversion     Inversion del gerente.
	 * @param str_propiedades   Propiedades del gerente.
	 * 
	 */

	public void vo_nuevoGerente(String str_nombre, String str_apellido,
			String str_dni, String str_email, int int_telefono,
			int int_rol, String str_contraseña,
			String str_nombreUsuario, String str_beneficio,
			String str_inversion, String str_propiedades) 
	{
		vo_conectar();
		try 
		{
			obj_ps = obj_conexion.prepareStatement(clsConstantesLD.
													SQL_INSERT_GERENTE);

			obj_ps.setString(1, str_nombre);
			obj_ps.setString(2, str_apellido);
			obj_ps.setString(3, str_dni);
			obj_ps.setString(4, str_email);
			obj_ps.setInt(5, int_telefono);
			obj_ps.setInt(6, int_rol);
			obj_ps.setString(7, str_contraseña);
			obj_ps.setString(8, str_nombreUsuario);
			obj_ps.setString(9, str_beneficio);
			obj_ps.setString(10, str_inversion);
			obj_ps.setString(11, str_propiedades);

			obj_ps.executeUpdate();
		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
			vo_desconectar();
		}
	}

	/**
	 * Metodo para recuperar la informacion de el gerente desde
	 * la base de datos.
	 * 
	 * @return ResultSet con la informacion de el gerente.
	 */
	public ResultSet arrl_recuperarInformacionGerente() 
	{

		vo_conectar();
		try 
		{
			obj_ps = obj_conexion.prepareStatement(clsConstantesLD.
													SQL_SELECT_GERENTE);

			obj_rs = obj_ps.executeQuery();
		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}

		return obj_rs;
	}

	/**
	 * Metodo para eliminar un gerente de la base de datos.
	 * 
	 * @param str_dni DNI del gerente a eliminar.
	 */
	public void vo_eliminarGerente(String str_dni) 
	{

		vo_conectar();

		try 
		{

			obj_ps = obj_conexion.prepareStatement(clsConstantesLD.
													SQL_DELETE_GERENTE);

			obj_ps.setString(3, str_dni);

			obj_ps.executeUpdate();

		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
			vo_desconectar();
		}
	}

	/**
	 * Metodo para modificar los datos de un Gerente en la base de datos.
	 * 
	 * @param str_nombre        Nuevo nombre del gerente.
	 * @param str_apellido      Nuevo apellido del gerente.
	 * @param str_dni           DNI del cliente a gerente.
	 * @param str_email         Nuevo email del gerente.
	 * @param int_telefono      Nuevo numero de telefono del gerente.
	 * @param int_rol           Nuevo rol del gerente.
	 * @param str_contraseña    Nueva contraseña del gerente.
	 * @param str_nombreUsuario Nuevo nombre de usuario del gerente.
	 * @param str_beneficio     Nuevo beneficio del gerente.
	 * @param str_inversion     Nueva inversion del gerente.
	 * @param str_propiedades   Nuevas propiedades del gerente.
	 */
	public void vo_modificarGerente(String str_nombre, String str_apellido,
			String str_dni, String str_email, int int_telefono,
			int int_rol, String str_contraseña,
			String str_nombreUsuario, String str_beneficio,
			String str_inversion, String str_propiedades) 
	{

		vo_conectar();
		try 
		{

			obj_ps = obj_conexion.prepareStatement(clsConstantesLD.
													SQL_UPDATE_GERENTE);

			obj_ps.setString(1, str_nombre);
			obj_ps.setString(2, str_apellido);
			obj_ps.setString(3, str_dni);
			obj_ps.setString(4, str_email);
			obj_ps.setInt(5, int_telefono);
			obj_ps.setInt(6, int_rol);
			obj_ps.setString(7, str_contraseña);
			obj_ps.setString(8, str_nombreUsuario);
			obj_ps.setString(9, str_beneficio);
			obj_ps.setString(10, str_inversion);
			obj_ps.setString(11, str_propiedades);

			obj_ps.executeUpdate();

		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
			vo_desconectar();
		}
	}

	/**
	 * Metodo para insertar un nuevo mecanico en la base de datos.
	 * 
	 * @param str_nombre        Nombre del mecanico.
	 * @param str_apellido      Apellido del mecanico.
	 * @param str_dni           DNI del mecanico.
	 * @param str_email         Email del mecanico.
	 * @param int_telefono      Telefono del mecanico.
	 * @param int_rol           Rol del mecanico.
	 * @param str_contraseña    Contraseña del mecanico.
	 * @param str_nombreUsuario Nombre de usuario del mecanico.
	 * @param str_puesto        Puesto del mecanico.
	 * @param str_sueldo        Sueldo del mecanico.
	 * 
	 */

	public void vo_nuevoMecanico(String str_nombre, String str_apellido,
			String str_dni, String str_email, int int_telefono, int int_rol,
			String str_contraseña, String str_nombreUsuario, String str_puesto,
			String str_sueldo) 
	{
		vo_conectar();
		try 
		{
			obj_ps = obj_conexion.prepareStatement(clsConstantesLD.
													SQL_INSERT_MECANICO);

			obj_ps.setString(1, str_nombre);
			obj_ps.setString(2, str_apellido);
			obj_ps.setString(3, str_dni);
			obj_ps.setString(4, str_email);
			obj_ps.setInt(5, int_telefono);
			obj_ps.setInt(6, int_rol);
			obj_ps.setString(7, str_contraseña);
			obj_ps.setString(8, str_nombreUsuario);
			obj_ps.setString(9, str_puesto);
			obj_ps.setString(10, str_sueldo);

			obj_ps.executeUpdate();
		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
			vo_desconectar();
		}
	}

	/**
	 * Metodo para recuperar la informacion de el mecancio
	 * desde la base de datos.
	 * 
	 * @return ResultSet con la informacion de el gerente.
	 */
	public ResultSet arrl_recuperarInformacionMecancio() 
	{

		vo_conectar();
		try 
		{
			obj_ps = obj_conexion.prepareStatement(clsConstantesLD.
													SQL_SELECT_MECANICO);
			obj_rs = obj_ps.executeQuery();
		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}

		return obj_rs;
	}

	/**
	 * Metodo para eliminar un mecanico de la base de datos.
	 * 
	 * @param str_dni DNI del mecanico a eliminar.
	 */
	public void vo_eliminarMecanico(String str_dni) 
	{

		vo_conectar();

		try 
		{

			obj_ps = obj_conexion.prepareStatement(clsConstantesLD.
													SQL_DELETE_MECANICO);

			obj_ps.setString(3, str_dni);

			obj_ps.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally 
		{
			vo_desconectar();
		}
	}

	/**
	 * Metodo para modificar los datos de un mecanico en la base de datos.
	 * 
	 * @param str_nombre        Nuevo mecanico del gerente.
	 * @param str_apellido      Nuevo mecanico del gerente.
	 * @param str_dni           DNI del mecanico a gerente.
	 * @param str_email         Nuevo email del mecanico.
	 * @param int_telefono      Nuevo numero de telefono del mecanico.
	 * @param int_rol           Nuevo rol del mecanico.
	 * @param str_contraseña    Nueva contraseña del mecanico.
	 * @param str_nombreUsuario Nuevo nombre de usuario del mecanico.
	 * @param str_puesto        Nuevo beneficio del mecanico.
	 * @param str_sueldo        Nueva inversion del mecanico.
	 */
	public void vo_modificarMecanico(String str_nombre, String str_apellido,
			String str_dni, String str_email, int int_telefono, int int_rol,
			String str_contraseña, String str_nombreUsuario, String str_puesto,
			String str_sueldo) 
	{

		vo_conectar();
		try 
		{

			obj_ps = obj_conexion.prepareStatement(clsConstantesLD.
													SQL_UPDATE_MECANICO);

			obj_ps.setString(1, str_nombre);
			obj_ps.setString(2, str_apellido);
			obj_ps.setString(3, str_dni);
			obj_ps.setString(4, str_email);
			obj_ps.setInt(5, int_telefono);
			obj_ps.setInt(6, int_rol);
			obj_ps.setString(7, str_contraseña);
			obj_ps.setString(8, str_nombreUsuario);
			obj_ps.setString(9, str_puesto);
			obj_ps.setString(10, str_sueldo);

			obj_ps.executeUpdate();

		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
			vo_desconectar();
		}
	}

	/**
	 * Metodo para eliminar un usuario de la base de datos.
	 * 
	 * @param str_param_dni DNI del usuario a eliminar.
	 */
	public void vo_eliminarUsuario(String str_param_dni) 
	{

		vo_conectar();
		try 
		{
			obj_ps = obj_conexion.prepareStatement(clsConstantesLD.
													SQL_DELETE_CLIENTE);
			obj_ps.setString(1, str_param_dni);
			obj_ps.executeUpdate();

		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
			vo_desconectar();
		}
	}


	/**
	 * Metodo para modificar los datos de un cliente en la base de datos.
	 * 
	 * @param str_nombre        Nuevo nombre del cliente.
	 * @param str_apellido      Nuevo apellido del cliente.
	 * @param str_dni           DNI del cliente a modificar.
	 * @param int_rol           Nuevo rol del cliente.
	 * @param str_user          Nuevo nombre de usuario del cliente.
	 * @param str_pass          Nueva contraseña del cliente.
	 */
	public void vo_modificarCliente(String str_dni, String str_nombre, 
			String str_apellido, int int_rol, String str_user, String str_pass) 
	{

		vo_conectar();
		try 
		{
			obj_ps = obj_conexion.prepareStatement(clsConstantesLD.
													SQL_UPDATE_CLIENTE);
			obj_ps.setString(1, str_nombre);
			obj_ps.setString(2, str_apellido);
			obj_ps.setInt(3, int_rol);	
			obj_ps.setString(4, str_user);
			obj_ps.setString(5, str_pass);
			obj_ps.setString(6, str_dni);
			

			obj_ps.executeUpdate();

		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
			vo_desconectar();
		}
	}

}